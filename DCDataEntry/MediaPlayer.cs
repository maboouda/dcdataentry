﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;



namespace DCDataEntry
{
    public partial class MediaPlayer : Form
    {
        string url = "";
        List<videoFrame> items = null;
        List<videoFrame> backupItems = null;
        public MediaPlayer(string filepath)
        {
            this.url = filepath;
            InitializeComponent();
            Player.URL = this.url;

            construct();
            //Player.uiMode = "none";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Player.close();
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Player.URL = @"D:\BA Stuff\NEMO.wmv";

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Player.Ctlcontrols.stop();

        }

        private void Player_MediaError(object sender, AxWMPLib._WMPOCXEvents_MediaErrorEvent e)
        {

        }
        private void construct()
        {
            items = new List<videoFrame>();
            items.Add(new videoFrame(1, "1111111"));
            items.Add(new videoFrame(2, "2222222"));
            items.Add(new videoFrame(3, "33333"));
            items.Add(new videoFrame(4, "444444"));
            items = items.OrderBy(o => o.FrameNumber).ToList();

            clearAndLoadVidsGrid();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                int fNum = Decimal.ToInt32(frameNumber.Value);
                if (frameInThere(fNum))
                {
                    MessageBox.Show(Constants.DuplicateFrameNumber);
                    return;
                }
                if (captionTB.Text.Trim() != String.Empty)
                {
                    items.Add(new videoFrame(fNum, captionTB.Text));
                }
                else
                {
                    MessageBox.Show(Constants.EmptyCaption);
                    return;
                }

                items = items.OrderBy(o => o.FrameNumber).ToList();
                clearAndLoadVidsGrid();

                //vidFrameGrid.DataSource = items;
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void clearAndLoadVidsGrid()
        {
            vidFrameGrid.DataSource = null;
            vidFrameGrid.Rows.Clear();

            BindingSource source = new BindingSource();
            source.DataSource = items;
            vidFrameGrid.DataSource = source;
            vidFrameGrid.Columns[0].ReadOnly = true;

        }

        private bool frameInThere(int frNum)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (((videoFrame)items[i]).FrameNumber == frNum)
                    return true;
            }
            return false;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            for (int i = 0; i < items.Count; i++)
            {
                MessageBox.Show(((videoFrame)items[i]).ToString());
            }
        }

        private void vidFrameGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
              /* if (e.ColumnIndex==0){
                   // frame number
                   DataGridViewCell cell = vidFrameGrid.Rows[e.RowIndex].Cells[e.ColumnIndex];
                   if (frameInThere(int.Parse(cell.Value.ToString())))
                   {
                      // frame already exists
                     
                       throw new Exception("Invalid Frame number");
                      
                   }
                 
                   clearAndLoadVidsGrid();
                 //  cell.Value

               }*/

            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.Message);

            }
       
        }

       

    }

    class videoFrame
    {
        int frameNumber;
        string caption;
        public videoFrame(int f, string val)
        {
            frameNumber = f;
            caption = val;
        }
        public int FrameNumber
        {
            get { return frameNumber; }
            set { frameNumber = value; }
        }
        public string Caption
        {
            get { return caption; }
            set { caption = value; }
        }
        public override string ToString()
        {
            return frameNumber + " " + caption;
        }

    }
}
