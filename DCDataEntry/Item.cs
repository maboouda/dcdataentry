﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

using MySql.Data.MySqlClient;

namespace DCDataEntry
{
    class Item
    {
        int itemID;
        int coll_ID;
        string[] files;
        string[] contributor;
        string[] publisher;
        string[] gener;
        string[] location;
        string[] subject;
        string[] timeperiod;
        string[] person;

        string collName;

        String title;
        string creator;
        String summary;
        DateTime pubDate;
        DateTime creationDate;

        
        string narrator;
        string mainLoc;
        string cpr;


        public bool updateDB()
        {
            MySqlDataReader dr = null;
            try
            {

                ArrayList param = new ArrayList();
                //new MySqlParameter("@role", 1)
                param.Add(new MySqlParameter("@title", title));
                param.Add(new MySqlParameter("@summary", summary));
                param.Add(new MySqlParameter("@pubDate", pubDate));
                param.Add(new MySqlParameter("@crDate", creationDate));
                param.Add(new MySqlParameter("@parent", collName));
                param.Add(new MySqlParameter("@files", arrToStr(files)));
                param.Add(new MySqlParameter("@contributor", arrToStr(contributor)));
                param.Add(new MySqlParameter("@publisher", arrToStr(publisher)));
                param.Add(new MySqlParameter("@gener", arrToStr(gener)));
                param.Add(new MySqlParameter("@location", arrToStr(location)));
                param.Add(new MySqlParameter("@subject", arrToStr(subject)));
                param.Add(new MySqlParameter("@timeperiod", arrToStr(timeperiod)));
                param.Add(new MySqlParameter("@person", arrToStr(person)));
                param.Add(new MySqlParameter("@creator", creator));
                param.Add(new MySqlParameter("@narrator", narrator));
                param.Add(new MySqlParameter("@mainLoc", mainLoc));
                param.Add(new MySqlParameter("@cpr", cpr));
                param.Add(new MySqlParameter("@srcItemID", itemID));

               dr =  MYSQL.MYSQLExecuteProcedure("Item_Create", MYSQL.getConnection(), param);
                while(dr.Read()){
                    itemID = int.Parse(dr["Item_ID"].ToString());
                }

                return true;

            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Item updaetDB error " + e.Message);
                return false;
            }
            finally
            {
                try
                {
                    dr.Close();
                }
                catch { }
            }
        }
        public bool deleteItem(int itemID)
        {
            try
            {
                ArrayList param = new ArrayList();
                param.Add(new MySqlParameter("@itemID", itemID));
                 MYSQL.MYSQLExecuteProcedure("Item_Delete", MYSQL.getConnection(), param).Close();
                return true;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Item load error " + e.Message);
                return false;
            }
        }
        public Item loadItem(int itemID)
        {
            MySqlDataReader dr = null;
            try
            {
                this.itemID = itemID;
                ArrayList param = new ArrayList();
                param.Add(new MySqlParameter("@itemID", itemID));
                dr = MYSQL.MYSQLExecuteProcedure("Item_Load", MYSQL.getConnection(), param);
                while (dr.Read())
                {

                    coll_ID = int.Parse(dr["Coll_ID"].ToString()); ;
                    files = strTOArr(dr["Files"].ToString());
                    contributor = strTOArr(dr["Contributor"].ToString());
                    publisher = strTOArr(dr["Publisher"].ToString());
                    gener = strTOArr(dr["Gener"].ToString());
                    location = strTOArr(dr["Location"].ToString());
                    subject = strTOArr(dr["Subject"].ToString());
                    timeperiod = strTOArr(dr["TimePeriod"].ToString()); 
                    person = strTOArr(dr["Person"].ToString());
                    try
                    {
                        this.creator = strTOArr(dr["Creator"].ToString())[0];
                    }
                    catch
                    {
                        this.creator = "";
                    }
                    try
                    {
                        this.narrator = strTOArr(dr["Narrator"].ToString())[0];
                    }
                    catch
                    {
                        this.narrator = "";
                    }
                    this.mainLoc = dr["L_Name"].ToString();
                    this.collName = dr["coll_Name"].ToString();
                    this.cpr = dr["CPR_Name"].ToString(); 
                    title = dr["Title"].ToString();
                    summary = dr["Summary"].ToString();
                    pubDate = DateTime.Parse(dr["PublishingDate"].ToString());
                    creationDate = DateTime.Parse(dr["CreationDate"].ToString());
                    
                   

                }
                return this;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Item load error " + e.Message);
                return null;
            }
            finally
            {
                try
                {
                    dr.Close();
                }
                catch { }
            }
        }
        public override String ToString(){
                return this.title;
            }
        private string[] strTOArr(string src)
        {
            string[] tmp;
            ArrayList arr = new ArrayList();
            try
            {
                tmp = src.Split('$');
                for (int i = 0; i < tmp.Length; i++)
                {
                    if (tmp[i].Trim() != String.Empty)
                    {
                        arr.Add(tmp[i].Trim());
                    }
                }
               tmp= arr.ToArray(typeof(string))as string[];
            }
            catch
            {
                tmp = null;
            }
            return tmp;
        }

        private String arrToStr(string[] arr)
        {
            if (arr == null || arr.Length == 0)
                return null;
            
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < arr.Length; i++)
            {
               if (arr[i].Trim()!=String.Empty){
                sb.Append(arr[i]);
                }else{
                   sb.Append("Empty");
               }
                sb.Append("$");
            }
            string result = sb.ToString();
            result = result.Substring(0, result.Length - 1);//.Replace("\\","\\\\");
            return result;
        }

        public string[] Files
        {
            get { return files; }
            set { files = value; }
        }

        public string[] Contributor
        {
            get { return contributor; }
            set { contributor = value; }
        }

        public string[] Publisher
        {
            get { return publisher; }
            set { publisher = value; }
        }

        public string[] Gener
        {
            get { return gener; }
            set { gener = value; }
        }

        public string[] Location
        {
            get { return location; }
            set { location = value; }
        }

        public string[] Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        public string[] Timeperiod
        {
            get { return timeperiod; }
            set { timeperiod = value; }
        }

        public string[] Person
        {
            get { return person; }
            set { person = value; }
        }

        public String Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Creator
        {
            get { return creator; }
            set { creator = value; }
        }

        public String Summary
        {
            get { return summary; }
            set { summary = value; }
        }

        public DateTime PublishDate
        {
            get { return pubDate; }
            set { pubDate = value; }
        }

        public DateTime CreationDate
        {
            get { return creationDate; }
            set { creationDate = value; }
        }

        public string Collection
        {
            get { return collName; }
            set { collName = value; }
        }




        public string Narrator
        {
            get { return narrator; }
            set { narrator = value; }
        }

        public string MainLocation
        {
            get { return mainLoc; }
            set { mainLoc = value; }
        }

        public int ItemID
        {
            get { return itemID; }
            set { itemID = value; }
        }

        public int CollID
        {
            get { return coll_ID; }
            set { coll_ID = value; }
        }

        public string Copyright
        {
            get { return cpr; }
            set { cpr = value; }
        }





        
    }
}

