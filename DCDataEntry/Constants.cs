﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCDataEntry
{
    class Constants
    {
        public static string addNew = "***Add New***";
        public static string contributorLabel = "Contributor: ";
        public static string publisherLabel = "Publisher: ";
        public static string generLabel = "Gener: ";
        public static string subjectLabel = "Subject: ";
        public static string timePeriodLabel = "Time Period: ";
        public static string locationLabel = "Location: ";
        public static string personsLabel = "Persons: ";
        public static string loadContributorPanelExp = "Error: loadContributorPanel ";
        public static string maxCountReached = "Max. Contributors  count = 5";
        public static string valueAlreadyExists = "The new Value already Exists in the list";
        public static string addedSucc = "Item is added Successfully!";
        public static string addederror = "Error while adding Item to Database";

        public static string EmptyFile = "Empty Files!";
        public static string incorrectFile = "Incorrect File path";
        public static string EmptyPersonName = "Empty Name!";

        public static string EmptyTitle = "Empty Title!";
        public static string EmptySum = "Empty Summary!";
        public static string EmptyCreator = "Empty Creator!";
        public static string EmptyCont = "Empty Contributor!";
        public static string EmptyNarrator = "Empty Narrator!";
        public static string EmptyMainLocation = "Empty Main Location!";
        public static string EmptyCopyright = "Empty Copyright!";
        
        public static string EmptyCollection = "Empty Collection!";
        public static string ParentAlreadyExists="Collection Already Exists with the same Name";
        public static string selectCollection = "Select the collection please";

        public static string loadError = "Error while loading information from Database";

        public static string PersonExists = "Person Name already Exists!";
        public static string DeleteError="Error while deleting the item";
        public static string selectItem="Select Item please";
        public static string Confirmation = "Are you sure?";
        public static string ItemIsNotSavedYet = "Item is not saved yet!";
        public static string FormatNotSupported = "File format is not supported!";
        public static string DuplicateFrameNumber = "Duplicate FrameNumber!";
        public static string EmptyCaption = "Empty Caption!";

        public static string TitleHint = "Title is used for display in search results and single item views. Recommended use is to reflect the content captured in digitized media files (such as the title of the piece performed or a short description of the content of a home movie). Lengthy titles may be truncated in the search results view.";
        public static string mainContributorHint="Main contributors are the primary persons or bodies associated with the creation of the content. Main contributors will be included in search results display and aggregated for browsing access. At this time there is no ability to specify a main contributor as a corporate body. When possible, use the Library of Congress Name Authority File.";
        public static string pubDateHint="Date should be the main publication date associated with the item to be used for sorting browse and search results. Enter date information in a format consistent with the options shown in Extended Date/Time Format (EDTF) 1.0.";
        public static string creationDateHint = "Creation date should only be used if Date is a re-issue date. Then Creation date would contain the original publication date. Enter date information in a format consistent with the options shown in Extended Date/Time Format (EDTF) 1.0.";
        public static string summaryHint = "Summary provides a space for describing the contents of the item. Examples include liner notes, contents list, or an opera scene abstract. This field is not meant for cataloger's descriptions but for descriptions that accompany the item. Depending on the length of the summary it may be truncated in search results.";
        public static string ContributorsHint="Contributors are persons or bodies associated with the item but not considered primary to the creation of its content. Examples of this would be performers in a band or opera, conductor, arranger, cinematographer, and choreographer. At this time this is no ability to specify a contributor as a corporate body. When possible, use the Library of Congress Name Authority File.";
        public static string publisherHint = "Publisher of the content of the item";
        public static string generHint = "Genre can be used to categorize an item by form, style, or subject matter. For consistency and to allow for sorting and aggregating, use terms from the Open Metadata Registry labels for PBCore: pbcoreGenre.";
        public static string subjectHint = "Subject should be used for the topical subject of the content. For consistency and to allow for sorting and aggregating, use terms from the Library of Congress Subject Headings. For temporal subjects, use the Time period(s) field and for geographic subjects, use the Location(s) field in this form.";
        public static string timePeriodHint = "Time period should be used for the temporal subject of the content (for example, years or year ranges). Enter date information in a format consistent with the options shown in Extended Date/Time Format (EDTF) 1.0.";
        public static string locationHint = "Location should be used for the geographic subject of the content. For consistency and to allow for sorting and aggregating, use terms from the Getty Thesaurus of Geographic Names.";
        public static string personHint = "Persons mentioned or being present in the item";

         public static string mainLocationHint ="The primary location where item had been collected, creatred, pictured,...etc";
         public static string copyrightHint = "Defines the level of copyright and access";
         public static string narratorHint ="Set the narrator. the narrator is the person who is commenting on the item";


         public static string SaveIsDone = "Exported Successfully!";
         public static string SaveIsNotDone = "Export Failed!";











        
    }
}
