﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.IO;
using System.Collections;
namespace DCDataEntry
{
    public class MYSQL
    {
        static private MySqlConnection myConn = null;
        static private MySqlDataAdapter myDa = null;
        static private string conStr = "";
        public static MySqlConnection getConnection()
        {
            try
            {
                if (myConn == null)
                {
                    string tmp = ConfigurationManager.AppSettings["ConnectionString"];


                   // string tmp =  "server=localhost; user id=root; password=; database=moc; pooling=false; charset=utf8";
                 //    tmp = "server=localhost; user id=root; password=mysql; database=moc; pooling=false; charset=utf8";
                    myConn = new MySqlConnection(tmp);
                
                    


                }
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("MySQL getConnection error " + e.Message);
                myConn = null;
            }
            return myConn;
        }
        public static DataSet MYSQLExecute(string query, MySqlConnection mycon)
        {
            DataSet s = null;
            try
            {
                myDa = new MySqlDataAdapter(query, mycon);
                s = new DataSet();
                myDa.Fill(s);
            }
            catch (Exception e)
            {
                return null;
            }
            return s;

        }
        public static string MYSQLExecuteScaler(string query, MySqlConnection mycon)
        {
            DataSet s = null;
            try
            {
                myDa = new MySqlDataAdapter(query, mycon);
                s = new DataSet();
                myDa.Fill(s);

            }
            catch
            {
                return "";
            }
            if (s != null && s.Tables[0].Rows.Count > 0)
                return s.Tables[0].Rows[0][0].ToString();
            else return "";

        }
        public static bool MYSQLExecuteNonQuery(string query, MySqlConnection mycon)
        {

            try
            {
                if (mycon.State == ConnectionState.Closed)
                {
                    mycon.Open();
                }
                MySqlCommand cmd = new MySqlCommand(query, mycon);
                cmd.ExecuteNonQuery();
                //  mycon.Close();

            }
            catch
            {
                return false;
            }

            return true;

        }
        public static MySqlDataReader MYSQLExecuteProcedure(string SPName, MySqlConnection mycon, ArrayList parametes)
        {

            MySqlDataReader rdr = null;
            try
            {
                
                if (mycon.State == ConnectionState.Closed)
                {
                    mycon.Open();
                }
                MySqlCommand cmd = new MySqlCommand(SPName, mycon);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (MySqlParameter p in parametes)
                {
                    cmd.Parameters.Add(p);
                }
                // cmd.Parameters.Add(new MySqlParameter("@CustomerID", custId));
                // execute the command
                rdr = cmd.ExecuteReader();

                // iterate through results, printing each to console
              /*  while (rdr.Read())
                {
                    Console.WriteLine(
                        "Product: {0,-25} Price: ${1,6:####.00}",
                        rdr["TenMostExpensiveProducts"],
                        rdr["UnitPrice"]);
                }*/
               

            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("MYSQLExecuteProcedure: " + SPName + " Error "+e.Message);
                System.Windows.Forms.MessageBox.Show("DB error "+e.Message);
                throw new Exception("MYSQLExecuteProcedure: " + SPName + " Error " + e.Message);
                
            }
            return rdr;

        }

        public static string[] readerToStringArr(MySqlDataReader rdr, string columnName)
        {
            try

            {
                ArrayList l = new ArrayList();
                
                  while (rdr.Read())
                  {

                      l.Add(rdr[columnName].ToString());
                          
                  }
                  rdr.Close();
                  string[] array = l.ToArray(typeof(string)) as string[];
                  return array;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("readerToStringArr: " +   " Error " + e.Message);
                return null;
            }
        }
    }
    


}
