﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Windows.Forms;
using System.IO;
namespace DCDataEntry
{
    class ModsWriter
    {

        public bool descMDBuilder(ModsObject item, string fileName, bool allowMessages)
        {
            XmlWriter writer = null;
            try
            {
                writer = XmlWriter.Create(item.ItemDir + "\\MODS\\" + fileName, new XmlWriterSettings { Indent = true, OmitXmlDeclaration = true });
                //1- write header
                writer.WriteStartElement("mods", "http://www.loc.gov/mods/v3");
                writer.WriteAttributeString("xsi", "schemaLocation", "http://www.w3.org/2001/XMLSchema-instance", "http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-4.xsd");

                //2- title
                writer.WriteStartElement("titleInfo");
                writer.WriteAttributeString("usage", "primary");
                writer.WriteElementString("title", item.Title);
                // title end
                writer.WriteEndElement();


                //3- Main Contributor
                writeMainContributor(writer, item);
                //4- Contributor
                writeContributor(writer, item);
                //5- Gener+OriginalInfo+Abstract
                writeOriginalInfo(writer, item);
                //6- Topic+Location+TimePeriod
                writeAllSubjectsInfo(writer, item);

                  //7- record info
                writeAllrecordInfo(writer, item);

                // MODS end element
                writer.WriteEndElement();
                writer.Flush();


            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Error in modsBuilder: " + e.Message);
                if(allowMessages)
                    MessageBox.Show(Constants.SaveIsNotDone);
                return false;
            }
            finally
            {
                try
                {
                    writer.Close();
                }
                catch
                {

                }

            }
            // convert to DC
            modsToDC(item.ItemDir + "\\MODS\\" + fileName, item.ItemDir + "\\DC\\" + fileName.Replace("mods","dc"), "XSLT\\MODS3-4_DC_XSLT2-0.xsl");
            // convert to marcxml
            modsToDC(item.ItemDir + "\\MODS\\" + fileName, item.ItemDir + "\\MARCXML\\" + fileName.Replace("mods", "marc"), "XSLT\\MODS3-4_MARC21slim_XSLT2-0.xsl");

            if (allowMessages)
            MessageBox.Show(Constants.SaveIsDone);



            return true;
        }
        public void modsToDC(string modsFile, string dcFile, string xsltfile)
        {
            try
            {
                XPathDocument myXPathDoc = new XPathDocument(modsFile);
                XslCompiledTransform myXslTrans = new XslCompiledTransform();
                myXslTrans.Load(xsltfile);
                XmlTextWriter myWriter = new XmlTextWriter(dcFile, Encoding.UTF8);
                myXslTrans.Transform(myXPathDoc, null, myWriter);


            }
            catch (Exception e)
            {

                LoggingWrapper.LoggingWrapper.Instance.Error("Error in converting mods to DC: " + e.Message);
                throw new Exception("Error in converting mods to DC: " + e.Message);

            }
        }
        private void writeMainContributor(XmlWriter writer, ModsObject item)
        {
            try
            {

                for (int i = 0; item.MainContributor != null && i < item.MainContributor.Length; i++)
                {

                    writer.WriteStartElement("name");
                    writer.WriteAttributeString("type", "personal");
                    writer.WriteAttributeString("usage", "primary");
                    writer.WriteElementString("namePart", item.MainContributor[i]);

                    writer.WriteStartElement("role");
                    writer.WriteStartElement("roleTerm");
                    writer.WriteAttributeString("authority", "marcrelator");
                    writer.WriteAttributeString("type", "code");
                    writer.WriteString("cre");
                    //role term end
                    writer.WriteEndElement();

                    writer.WriteStartElement("roleTerm");
                    writer.WriteAttributeString("authority", "marcrelator");
                    writer.WriteAttributeString("type", "text");
                    writer.WriteString("Creator");
                    //role term end
                    writer.WriteEndElement();
                    // role end
                    writer.WriteEndElement();
                    // name END
                    writer.WriteEndElement();

                }

            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Error in writeMainContributor: " + e.Message);
                throw new Exception("Error in writeMainContributor: " + e.Message);

            }

        }
        private void writeContributor(XmlWriter writer, ModsObject item)
        {
            try
            {

                for (int i = 0; item.Contributor != null && i < item.Contributor.Length; i++)
                {

                    writer.WriteStartElement("name");
                    writer.WriteAttributeString("type", "personal");
                    writer.WriteElementString("namePart", item.Contributor[i]);

                    writer.WriteStartElement("role");
                    writer.WriteStartElement("roleTerm");
                    writer.WriteAttributeString("authority", "marcrelator");
                    writer.WriteAttributeString("type", "code");
                    writer.WriteString("ctb");
                    //role term end
                    writer.WriteEndElement();

                    writer.WriteStartElement("roleTerm");
                    writer.WriteAttributeString("authority", "marcrelator");
                    writer.WriteAttributeString("type", "text");
                    writer.WriteString("Contributor");
                    //role term end
                    writer.WriteEndElement();
                    // role end
                    writer.WriteEndElement();
                    // name END
                    writer.WriteEndElement();

                }
                // write narrator

                writer.WriteStartElement("name");
                writer.WriteAttributeString("type", "personal");
                writer.WriteElementString("namePart", item.Narrator);

                writer.WriteStartElement("role");
                writer.WriteStartElement("roleTerm");
                writer.WriteAttributeString("authority", "marcrelator");
                writer.WriteAttributeString("type", "code");
                writer.WriteString("nrt");
                //role term end
                writer.WriteEndElement();

                writer.WriteStartElement("roleTerm");
                writer.WriteAttributeString("authority", "marcrelator");
                writer.WriteAttributeString("type", "text");
                writer.WriteString("Narrator");
                //role term end
                writer.WriteEndElement();
                // role end
                writer.WriteEndElement();
                // name END
                writer.WriteEndElement();


            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Error in writeContributor: " + e.Message);
                throw new Exception("Error in writeContributor: " + e.Message);

            }


        }
        private void writeOriginalInfo(XmlWriter writer, ModsObject item)
        {
            try
            {
                // gener
                for (int i = 0;item.Gener!=null && i < item.Gener.Length; i++)
                {
                    writer.WriteElementString("genre", item.Gener[i]);
                 }
                // originalInfo
                   writer.WriteStartElement("originInfo");
                   // date issued
                   if (item.DateIssued != null && item.DateIssued.Trim() != String.Empty)
                   { 
                         writer.WriteStartElement("dateIssued");
                         writer.WriteAttributeString("encoding", "edtf");
                         writer.WriteString(item.DateIssued);
                         writer.WriteEndElement();
                    }

                    // date created
                    if (item.DateCreated != null && item.DateCreated.Trim() != String.Empty)
                    {
                        writer.WriteStartElement("dateCreated");
                        writer.WriteAttributeString("encoding", "edtf");
                        writer.WriteString(item.DateCreated);
                        writer.WriteEndElement();

                    }

                //publisher
                    for (int i = 0; item.Publisher != null && i < item.Publisher.Length; i++)
                    {
                        writer.WriteElementString("publisher", item.Publisher[i]);
                    }
                // original info end
                writer.WriteEndElement();

                // abstract
                if (item.AbstractStr != null && item.AbstractStr.Trim() != String.Empty)
                {

                    writer.WriteElementString("abstract", item.AbstractStr);
                }


        
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Error in writeOriginalInfo: " + e.Message);
                throw new Exception("Error in writeOriginalInfo: " + e.Message);

            }


        }
        private void writeAllSubjectsInfo(XmlWriter writer, ModsObject item)
        {
            try
            {
                // Topic
                for (int i = 0; item.Subject != null && i < item.Subject.Length; i++)
                {
                    writer.WriteStartElement("subject");
                    writer.WriteElementString("topic", item.Subject[i]);
                    writer.WriteEndElement();

                }

                // TimePeriod
                for (int i = 0; item.TimePeriod!=null && i < item.TimePeriod.Length; i++)
                {
                    writer.WriteStartElement("subject");
                    writer.WriteElementString("temporal", item.TimePeriod[i]);
                    writer.WriteEndElement();

                }

                // Location
                for (int i = 0; item.Location != null && i < item.Location.Length; i++)
                {
                    writer.WriteStartElement("subject");
                    writer.WriteElementString("geographic", item.Location[i]);
                    writer.WriteEndElement();

                }
               

            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Error in writeAllSubjectsInfo: " + e.Message);
                throw new Exception("Error in writeAllSubjectsInfo: " + e.Message);

            }


        }
        private void writeAllrecordInfo(XmlWriter writer, ModsObject item)
        {
            try
            {

                writer.WriteStartElement("recordInfo");
                     writer.WriteElementString("recordOrigin","MOC DataEntry Tool. QNL");
                     writer.WriteElementString("recordContentSource", "Ministry Of Culture MOC, QATAR");
                     writer.WriteElementString("recordCreationDate", DateTime.Now.ToShortDateString());
                     writer.WriteElementString("recordChangeDate",  DateTime.Now.ToUniversalTime().ToString());

                    writer.WriteStartElement("recordIdentifier");
                     writer.WriteAttributeString("source", "DCTool");
                      writer.WriteString(item.ID.ToString());
                    writer.WriteEndElement();

                writer.WriteStartElement("languageOfCataloging");
                    writer.WriteStartElement("languageTerm");
                         writer.WriteAttributeString("authority", "iso639-2b");
                         writer.WriteAttributeString("type", "code");
                         writer.WriteString("ara");
                    writer.WriteEndElement();
                 writer.WriteEndElement();
                // recordInfo end
                writer.WriteEndElement();


                // write access
                writer.WriteStartElement("accessCondition");
                writer.WriteAttributeString("type", "restrictionOnAccess");
                writer.WriteString(item.Copyright);
                writer.WriteEndElement();

              


            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Error in writeAllrecordInfo: " + e.Message);
                throw new Exception("Error in writeAllrecordInfo: " + e.Message);

            }


        }
    }

    class ModsObject
    {
        String title;
        String narrator;
        String copyright;
        string[] mainContributor;
        string[] contributor;
        string[] gener;
        string[] publisher;
        string[] subject;
        string[] timePeriod;
        string[] location;
        string dateIssued;
        string dateCreated;
        String abstractStr;
        int itemID;

        string itemDir = "";

        public ModsObject(Item item)
        {

            this.title = item.Title;
            this.narrator = item.Narrator;
            this.copyright = item.Copyright;
            this.mainContributor = new string[] {item.Creator};
            this.contributor = new string[item.Contributor.Length+item.Person.Length];
            item.Contributor.CopyTo(this.contributor, 0);
            item.Person.CopyTo(this.contributor, item.Contributor.Length);
            this.gener = item.Gener;
            this.publisher = item.Publisher;
            this.subject = item.Subject;
            this.timePeriod = item.Timeperiod;
            this.location = new string[item.Location.Length+1];
            this.location[0] = item.MainLocation;
            item.Location.CopyTo(this.location, 1);
            this.dateIssued = item.PublishDate.ToString();
            this.dateCreated = item.CreationDate.ToString();
            this.abstractStr = item.Summary;
            this.itemID = item.ItemID;

            


        }

        public int ID
        {
            get { return itemID; }
            set { itemID = value; }
        }
        public String Title
        {
            get { return title; }
            set { title = value; }
        }
        public String Narrator
        {
            get { return narrator; }
            set { narrator = value; }
        }
        public String Copyright
        {
            get { return copyright; }
            set { copyright = value; }
        }
        public string[] MainContributor
        {
            get { return mainContributor; }
            set { mainContributor = value; }
        }
        public string[] Contributor
        {
            get { return contributor; }
            set { contributor = value; }
        }
        public string[] Gener
        {
            get { return gener; }
            set { gener = value; }
        }
        public string[] Publisher
        {
            get { return publisher; }
            set { publisher = value; }
        }
        public string[] Subject
        {
            get { return subject; }
            set { subject = value; }
        }
        public string[] TimePeriod
        {
            get { return timePeriod; }
            set { timePeriod = value; }
        }
        public string[] Location
        {
            get { return location; }
            set { location = value; }
        }
        public string DateIssued
        {
            get { return dateIssued; }
            set { dateIssued = value; }
        }
        public string DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }
        public string AbstractStr
        {
            get { return abstractStr; }
            set { abstractStr = value; }
        }
        public string ItemDir
        {
            get { return itemDir; }
            set { itemDir = value; }
        }
        public void createDirectories()
        {
            //   Directory.Delete(this.ItemDir, true);
            Directory.CreateDirectory(this.ItemDir);
            Directory.CreateDirectory(this.ItemDir + "\\MODS");
            Directory.CreateDirectory(this.ItemDir + "\\DC");
            Directory.CreateDirectory(this.ItemDir + "\\MARCXML");

        }



    }
}
