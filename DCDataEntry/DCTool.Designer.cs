﻿namespace DCDataEntry
{
    partial class DCTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DCTool));
            this.langCB = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.config3 = new System.Windows.Forms.Button();
            this.config2 = new System.Windows.Forms.Button();
            this.config1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Warn3 = new System.Windows.Forms.Label();
            this.Play3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.Warn2 = new System.Windows.Forms.Label();
            this.Play2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.warningLBL = new System.Windows.Forms.Label();
            this.Play1 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.contributorPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.narratorB = new System.Windows.Forms.Button();
            this.narratorCB = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Summary = new System.Windows.Forms.Label();
            this.sumTextBox = new System.Windows.Forms.TextBox();
            this.CreatorDDL = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TitleTB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.mainLocationCB = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.subjectPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.personPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.locationPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.TimePersiodPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.generPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.publisherPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.saveButton = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cprDDL = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.exportButton = new System.Windows.Forms.Button();
            this.exportAll = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // langCB
            // 
            resources.ApplyResources(this.langCB, "langCB");
            this.langCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.langCB.FormattingEnabled = true;
            this.langCB.Items.AddRange(new object[] {
            resources.GetString("langCB.Items"),
            resources.GetString("langCB.Items1")});
            this.langCB.Name = "langCB";
            this.langCB.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.config3);
            this.groupBox1.Controls.Add(this.config2);
            this.groupBox1.Controls.Add(this.config1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Warn3);
            this.groupBox1.Controls.Add(this.Play3);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.Warn2);
            this.groupBox1.Controls.Add(this.Play2);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.warningLBL);
            this.groupBox1.Controls.Add(this.Play1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // config3
            // 
            this.config3.Image = global::DCDataEntry.Properties.Resources.configure;
            resources.ApplyResources(this.config3, "config3");
            this.config3.Name = "config3";
            this.config3.UseVisualStyleBackColor = true;
            this.config3.Click += new System.EventHandler(this.config3_Click);
            // 
            // config2
            // 
            this.config2.Image = global::DCDataEntry.Properties.Resources.configure;
            resources.ApplyResources(this.config2, "config2");
            this.config2.Name = "config2";
            this.config2.UseVisualStyleBackColor = true;
            this.config2.Click += new System.EventHandler(this.config2_Click);
            // 
            // config1
            // 
            this.config1.Image = global::DCDataEntry.Properties.Resources.configure;
            resources.ApplyResources(this.config1, "config1");
            this.config1.Name = "config1";
            this.config1.UseVisualStyleBackColor = true;
            this.config1.Click += new System.EventHandler(this.config1_Click);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // Warn3
            // 
            resources.ApplyResources(this.Warn3, "Warn3");
            this.Warn3.ForeColor = System.Drawing.Color.Maroon;
            this.Warn3.Name = "Warn3";
            // 
            // Play3
            // 
            this.Play3.Image = global::DCDataEntry.Properties.Resources._1417520210_black_4_audio_play_16;
            resources.ApplyResources(this.Play3, "Play3");
            this.Play3.Name = "Play3";
            this.Play3.UseVisualStyleBackColor = true;
            this.Play3.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBox3
            // 
            resources.ApplyResources(this.textBox3, "textBox3");
            this.textBox3.Name = "textBox3";
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // Warn2
            // 
            resources.ApplyResources(this.Warn2, "Warn2");
            this.Warn2.ForeColor = System.Drawing.Color.Maroon;
            this.Warn2.Name = "Warn2";
            // 
            // Play2
            // 
            this.Play2.Image = global::DCDataEntry.Properties.Resources._1417520210_black_4_audio_play_16;
            resources.ApplyResources(this.Play2, "Play2");
            this.Play2.Name = "Play2";
            this.Play2.UseVisualStyleBackColor = true;
            this.Play2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox2
            // 
            resources.ApplyResources(this.textBox2, "textBox2");
            this.textBox2.Name = "textBox2";
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // warningLBL
            // 
            resources.ApplyResources(this.warningLBL, "warningLBL");
            this.warningLBL.ForeColor = System.Drawing.Color.Maroon;
            this.warningLBL.Name = "warningLBL";
            // 
            // Play1
            // 
            this.Play1.Image = global::DCDataEntry.Properties.Resources._1417520210_black_4_audio_play_16;
            resources.ApplyResources(this.Play1, "Play1");
            this.Play1.Name = "Play1";
            this.Play1.UseVisualStyleBackColor = true;
            this.Play1.Click += new System.EventHandler(this.Play1_Click);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // treeView1
            // 
            resources.ApplyResources(this.treeView1, "treeView1");
            this.treeView1.HideSelection = false;
            this.treeView1.Name = "treeView1";
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.contributorPanel);
            this.groupBox2.Controls.Add(this.narratorB);
            this.groupBox2.Controls.Add(this.narratorCB);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.Summary);
            this.groupBox2.Controls.Add(this.sumTextBox);
            this.groupBox2.Controls.Add(this.CreatorDDL);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.TitleTB);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // contributorPanel
            // 
            resources.ApplyResources(this.contributorPanel, "contributorPanel");
            this.contributorPanel.Name = "contributorPanel";
            // 
            // narratorB
            // 
            this.narratorB.Image = global::DCDataEntry.Properties.Resources.configure_2;
            resources.ApplyResources(this.narratorB, "narratorB");
            this.narratorB.Name = "narratorB";
            this.narratorB.UseVisualStyleBackColor = true;
            this.narratorB.Click += new System.EventHandler(this.narratorB_Click);
            // 
            // narratorCB
            // 
            this.narratorCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.narratorCB.FormattingEnabled = true;
            resources.ApplyResources(this.narratorCB, "narratorCB");
            this.narratorCB.Name = "narratorCB";
            this.narratorCB.SelectedIndexChanged += new System.EventHandler(this.narratorCB_SelectedIndexChanged);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // Summary
            // 
            resources.ApplyResources(this.Summary, "Summary");
            this.Summary.Name = "Summary";
            // 
            // sumTextBox
            // 
            resources.ApplyResources(this.sumTextBox, "sumTextBox");
            this.sumTextBox.Name = "sumTextBox";
            // 
            // CreatorDDL
            // 
            this.CreatorDDL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CreatorDDL.FormattingEnabled = true;
            resources.ApplyResources(this.CreatorDDL, "CreatorDDL");
            this.CreatorDDL.Name = "CreatorDDL";
            this.CreatorDDL.SelectedIndexChanged += new System.EventHandler(this.CreatorDDL_SelectedIndexChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // TitleTB
            // 
            resources.ApplyResources(this.TitleTB, "TitleTB");
            this.TitleTB.Name = "TitleTB";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // mainLocationCB
            // 
            this.mainLocationCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mainLocationCB.FormattingEnabled = true;
            resources.ApplyResources(this.mainLocationCB, "mainLocationCB");
            this.mainLocationCB.Name = "mainLocationCB";
            this.mainLocationCB.SelectedIndexChanged += new System.EventHandler(this.mainLocationCB_SelectedIndexChanged);
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // dateTimePicker2
            // 
            resources.ApplyResources(this.dateTimePicker2, "dateTimePicker2");
            this.dateTimePicker2.Name = "dateTimePicker2";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // dateTimePicker1
            // 
            resources.ApplyResources(this.dateTimePicker1, "dateTimePicker1");
            this.dateTimePicker1.Name = "dateTimePicker1";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // subjectPanel
            // 
            resources.ApplyResources(this.subjectPanel, "subjectPanel");
            this.subjectPanel.Name = "subjectPanel";
            // 
            // groupBox3
            // 
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Controls.Add(this.subjectPanel);
            this.groupBox3.Controls.Add(this.personPanel);
            this.groupBox3.Controls.Add(this.locationPanel);
            this.groupBox3.Controls.Add(this.TimePersiodPanel);
            this.groupBox3.Controls.Add(this.generPanel);
            this.groupBox3.Controls.Add(this.publisherPanel);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // personPanel
            // 
            resources.ApplyResources(this.personPanel, "personPanel");
            this.personPanel.Name = "personPanel";
            // 
            // locationPanel
            // 
            resources.ApplyResources(this.locationPanel, "locationPanel");
            this.locationPanel.Name = "locationPanel";
            // 
            // TimePersiodPanel
            // 
            resources.ApplyResources(this.TimePersiodPanel, "TimePersiodPanel");
            this.TimePersiodPanel.Name = "TimePersiodPanel";
            // 
            // generPanel
            // 
            resources.ApplyResources(this.generPanel, "generPanel");
            this.generPanel.Name = "generPanel";
            // 
            // publisherPanel
            // 
            resources.ApplyResources(this.publisherPanel, "publisherPanel");
            this.publisherPanel.Name = "publisherPanel";
            // 
            // saveButton
            // 
            resources.ApplyResources(this.saveButton, "saveButton");
            this.saveButton.Name = "saveButton";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // clear
            // 
            resources.ApplyResources(this.clear, "clear");
            this.clear.Name = "clear";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // textBox4
            // 
            resources.ApplyResources(this.textBox4, "textBox4");
            this.textBox4.Name = "textBox4";
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // groupBox4
            // 
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Controls.Add(this.mainLocationCB);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.cprDDL);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.dateTimePicker1);
            this.groupBox4.Controls.Add(this.dateTimePicker2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // cprDDL
            // 
            this.cprDDL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cprDDL.FormattingEnabled = true;
            resources.ApplyResources(this.cprDDL, "cprDDL");
            this.cprDDL.Name = "cprDDL";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // exportButton
            // 
            resources.ApplyResources(this.exportButton, "exportButton");
            this.exportButton.Name = "exportButton";
            this.exportButton.UseVisualStyleBackColor = true;
            this.exportButton.Click += new System.EventHandler(this.exportButton_Click);
            // 
            // exportAll
            // 
            resources.ApplyResources(this.exportAll, "exportAll");
            this.exportAll.Name = "exportAll";
            this.exportAll.UseVisualStyleBackColor = true;
            this.exportAll.Click += new System.EventHandler(this.exportAll_Click);
            // 
            // DCTool
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.exportAll);
            this.Controls.Add(this.exportButton);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.langCB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "DCTool";
            this.ShowIcon = false;
            this.Load += new System.EventHandler(this.DCTool_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox langCB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TitleTB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox CreatorDDL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.FlowLayoutPanel publisherPanel;
        private System.Windows.Forms.FlowLayoutPanel generPanel;
        private System.Windows.Forms.FlowLayoutPanel subjectPanel;
        private System.Windows.Forms.FlowLayoutPanel locationPanel;
        private System.Windows.Forms.FlowLayoutPanel TimePersiodPanel;
        private System.Windows.Forms.FlowLayoutPanel personPanel;
        private System.Windows.Forms.TextBox sumTextBox;
        private System.Windows.Forms.Label Summary;
        private System.Windows.Forms.Button Play1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label warningLBL;
        private System.Windows.Forms.Label Warn3;
        private System.Windows.Forms.Button Play3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label Warn2;
        private System.Windows.Forms.Button Play2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button config1;
        private System.Windows.Forms.Button config3;
        private System.Windows.Forms.Button config2;
        private System.Windows.Forms.ComboBox narratorCB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button narratorB;
        private System.Windows.Forms.ComboBox mainLocationCB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cprDDL;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.FlowLayoutPanel contributorPanel;
        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.Button exportAll;
    }
}