﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using MySql.Data.MySqlClient;

namespace DCDataEntry
{
    class DataLoader
    {
        public enum DataSource { Contributor = 1, Publisher = 2, Subject = 3, Location = 4, Gener = 5, TimePeriod = 6, Person = 7 , Creator=8, Narrator=9};
        static public string[] contributorArr;
        static public string[] PublisherArr;
        static public string[] CreatorArr;
        static public string[] NarratorArr;
        static public string[] SubjectArr;
        static public string[] LocationArr;
        static public string[] GenerArr;
        static public string[] TimePeriodArr;
        static public string[] PersonArr;
        static public string[] countryArr;
        static public string[] cprArr;
        static private string[] pivot = new string[] { "", Constants.addNew };
    
        
        private static string[] loadHuman(DataSource type)
        {
            try
            {
                int personRole = 0;
                string[] obj = null;
                switch (type){
                    case DataSource.Contributor:
                        personRole=1;
                        obj = contributorArr;
                        break;
                    case DataSource.Publisher:
                        personRole = 2;
                        obj = PublisherArr;
                        break;
                    case DataSource.Person:
                        personRole = 3;
                        obj = PersonArr;
                        break;
                    case DataSource.Creator:
                        personRole = 4;
                        obj = CreatorArr;
                        break;
                    case DataSource.Narrator:
                        personRole = 5;
                        obj = NarratorArr;
                        break;

                }

       

                    ArrayList param = new ArrayList();
                    MySqlParameter p1 = new MySqlParameter("@personRole", personRole);
                    param.Add(p1);

                    obj = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("person_loadall", MYSQL.getConnection(), param)), "P_Name");
                    obj = mergeArrayes(pivot, obj);
                

                switch (type)
                {
                    case DataSource.Contributor:
                        contributorArr= obj;
                        break;
                    case DataSource.Publisher:
                        PublisherArr=obj ;
                        break;
                    case DataSource.Person:
                        PersonArr=obj;
                        break;
                    case DataSource.Creator:
                        CreatorArr=obj ;
                        break;
                    case DataSource.Narrator:
                        NarratorArr=obj ;
                        break;

                }

                return obj;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("loadContributor: " + "Error " + e.Message);
                return null;

            }
        }
        public static string[] loadContributor()
        {
            return loadHuman(DataSource.Contributor);
        }
        public static string[] loadPublisher()
        {
            return loadHuman(DataSource.Publisher);
        }
        public static string[] loadCreator()
        {
            return loadHuman(DataSource.Creator);
        }
        public static string[] loadPerson()
        {
            return loadHuman(DataSource.Person);
        }
        public static string[] loadNarrator()
        {
            return loadHuman(DataSource.Narrator);
        }
        public static string[] loadSubject()
        {
            try
            {


                if (SubjectArr == null)
                {

                    ArrayList param = new ArrayList();
                    SubjectArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("Subject_loadAll", MYSQL.getConnection(), param)), "S_Name");
                    SubjectArr = mergeArrayes(pivot, SubjectArr);
                }

                return SubjectArr;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("loadSubject: " + "Error " + e.Message);
                return null;

            }
        }
        public static string[] loadCountry()
        {
            try
            {


                if (countryArr == null)
                {

                    ArrayList param = new ArrayList();
                    countryArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("Nationality_loadAll", MYSQL.getConnection(), param)), "N_Caption");
                }

                return countryArr;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("loadCountry: " + "Error " + e.Message);
                return null;

            }
        }
        public static string[] loadCopyright()
        {
            try
            {


                if (cprArr == null)
                {

                    ArrayList param = new ArrayList();
                    cprArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("CopyRight_loadAll", MYSQL.getConnection(), param)), "CPR_Name");
                }

                return cprArr;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("cprArr: " + "Error " + e.Message);
                return null;

            }
        }
        public static string[] loadLocation()
        {
            try
            {
                
                if (LocationArr == null)
                {

                    ArrayList param = new ArrayList();
                    LocationArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("Location_loadAll", MYSQL.getConnection(), param)), "L_Name");
                    LocationArr = mergeArrayes(pivot, LocationArr);
                }

                return LocationArr;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("loadLocation: " + "Error " + e.Message);
                return null;

            }
        }
        public static string[] loadGener()
        {
            try
            {

                if (GenerArr == null)
                {

                    ArrayList param = new ArrayList();
                    GenerArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("Gener_loadAll", MYSQL.getConnection(), param)), "G_Name");
                    GenerArr = mergeArrayes(pivot, GenerArr);
                }

                return GenerArr;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("loadGener: " + "Error " + e.Message);
                return null;

            }
        }
        public static string[] loadTimePeriod()
        {
            try
            {

                if (TimePeriodArr == null)
                {

                    ArrayList param = new ArrayList();
                    TimePeriodArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("TimePeriod_loadAll", MYSQL.getConnection(), param)), "TP_Name");
                    TimePeriodArr = mergeArrayes(pivot, TimePeriodArr);
                }

                return TimePeriodArr;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("loadTimePeriod: " + "Error " + e.Message);
                return null;

            }
        }
        public static string[] addItem(DataSource arrType,string value)
        {
            string[] objects = null;

            try
            {
                switch (arrType)
                {
                    case DataSource.Contributor:
                        objects = contributorArr;
                        break;

                    case DataSource.Publisher:
                        objects = PublisherArr;
                        break;
                    case DataSource.Subject:
                        objects = SubjectArr;
                        break;
                    case DataSource.Location:
                        objects = LocationArr;
                        break;
                    case DataSource.Gener:
                        objects = GenerArr;
                        break;
                    case DataSource.TimePeriod:
                        objects = TimePeriodArr;
                        break;
                    case DataSource.Person:
                        objects = PersonArr;
                        break;
                    case DataSource.Creator:
                        objects = CreatorArr;
                        break;
                    case DataSource.Narrator:
                        objects = NarratorArr;
                        break;

                }
                string[] tmp = new string[] { value };
               /* int array1OriginalLength = objects.Length;
                Array.Resize<string>(ref objects, array1OriginalLength + tmp.Length);
                Array.Copy(tmp, 0, objects, array1OriginalLength, tmp.Length);*/
                // flush to DB
                objects = mergeArrayes(objects, tmp);
                // flush to DB
                flushtoDB(arrType, value);

                switch (arrType)
                {
                    case DataSource.Contributor:
                        contributorArr = objects;
                        break;

                    case DataSource.Publisher:
                        PublisherArr = objects;
                        break;
                    case DataSource.Subject:
                        SubjectArr = objects;
                        break;
                    case DataSource.Location:
                        LocationArr = objects;
                        break;
                    case DataSource.Gener:
                        GenerArr = objects;
                        break;
                    case DataSource.TimePeriod:
                        TimePeriodArr = objects;
                        break;
                    case DataSource.Person:
                        PersonArr = objects;
                        break;
                    case DataSource.Creator:
                        CreatorArr = objects;
                        break;
                    case DataSource.Narrator:
                        NarratorArr=objects;
                        break;
                }
            }
            catch (Exception e)
            {
                objects = null;
            }
            return objects;

        }
        private static string[] mergeArrayes(string[] arr1, string[] arr2)
        {
            string[] objects = arr1;

            int array1OriginalLength = arr1.Length;
            Array.Resize<string>(ref objects, array1OriginalLength + arr2.Length);
            Array.Copy(arr2, 0, objects, array1OriginalLength, arr2.Length);
            return objects;

        }
       private static void flushtoDB(DataSource type, string value) {
       try{
           string spName = "";
           ArrayList param = new ArrayList();
           param.Add(new MySqlParameter("@val", value));

           switch (type)
            {
                case DataSource.Contributor:
                    spName = "Person_Add";
                    param.Add(new MySqlParameter("@role", 1));
                   break;

                case DataSource.Publisher:
                    spName = "Person_Add";
                    param.Add(new MySqlParameter("@role", 2));
                    break;

                case DataSource.Creator:
                    spName = "Person_Add";
                    param.Add(new MySqlParameter("@role", 4));
                    break;
                case DataSource.Narrator:
                    spName = "Person_Add";
                    param.Add(new MySqlParameter("@role", 5));
                    break;
                case DataSource.Subject:
                    spName = "Subject_Add";
                    break;
                case DataSource.Location:
                    spName = "Location_Add";
                    break;
                case DataSource.Gener:
                    spName = "Gener_Add";
                    break;
                case DataSource.TimePeriod:
                    spName = "TimePeriod_Add";
                    break;
                case DataSource.Person:
                    spName = "Person_Add";
                    param.Add(new MySqlParameter("@role", 3));
                    break;
            }

           MYSQL.MYSQLExecuteProcedure(spName, MYSQL.getConnection(), param).Close();

       }catch(Exception e){
           LoggingWrapper.LoggingWrapper.Instance.Error("Flush to DB Error :"+e.Message);
          // System.Windows.Forms.MessageBox.Show("Flush to DB: " + e.Message);
           throw e;
       }
   }

        /*     public static string[] loadContributor()
       {
           try
           {


               if (contributorArr == null)
               {

                   //contributorArr = new string[] { "", Constants.addNew, "Reuters", "MOC","abc" };
                   ArrayList param = new ArrayList();
                   MySqlParameter p1 = new MySqlParameter("@personRole", 1);
                   param.Add(p1);

                   contributorArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("person_loadall", MYSQL.getConnection(), param)), "P_Name");
                   contributorArr = mergeArrayes(pivot, contributorArr);
               }

               // Array.Sort(contributorArr);
               return contributorArr;
           }
           catch (Exception e)
           {
               LoggingWrapper.LoggingWrapper.Instance.Error("loadContributor: " + "Error " + e.Message);
               return null;

           }
       }
       public static string[] loadPublisher()
       {
           try
           {


               if (PublisherArr == null)
               {

                   ArrayList param = new ArrayList();
                   MySqlParameter p1 = new MySqlParameter("@personRole", 2);
                   param.Add(p1);

                   PublisherArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("person_loadall", MYSQL.getConnection(), param)), "P_Name");
                   PublisherArr = mergeArrayes(pivot, PublisherArr);
               }

               return PublisherArr;
           }
           catch (Exception e)
           {
               LoggingWrapper.LoggingWrapper.Instance.Error("loadPublisher: " + "Error " + e.Message);
               return null;

           }
       }

       public static string[] loadCreator()
       {
           try
           {


               if (CreatorArr == null)
               {

                   ArrayList param = new ArrayList();
                   MySqlParameter p1 = new MySqlParameter("@personRole", 4);
                   param.Add(p1);

                   CreatorArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("person_loadall", MYSQL.getConnection(), param)), "P_Name");
                   CreatorArr = mergeArrayes(pivot, CreatorArr);
               }

               return CreatorArr;
           }
           catch (Exception e)
           {
               LoggingWrapper.LoggingWrapper.Instance.Error("loadCreator: " + "Error " + e.Message);
               return null;

           }
       }
       public static string[] loadPerson()
       {
           try
           {


               if (PersonArr == null)
               {

                   ArrayList param = new ArrayList();
                   MySqlParameter p1 = new MySqlParameter("@personRole", 3);
                   param.Add(p1);

                   PersonArr = MYSQL.readerToStringArr((MYSQL.MYSQLExecuteProcedure("person_loadall", MYSQL.getConnection(), param)), "P_Name");
                   PersonArr = mergeArrayes(pivot, PersonArr);
               }

               return PersonArr;
           }
           catch (Exception e)
           {
               LoggingWrapper.LoggingWrapper.Instance.Error("loadPerson: " + "Error " + e.Message);
               return null;

           }
       }*/
    
    }
}
