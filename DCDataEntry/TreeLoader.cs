﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DCDataEntry
{
    class TreeLoader
    {

        static ArrayList srcNodes = null;
        static public ArrayList SrcNodes
        {
            get { return srcNodes; }
            set { srcNodes = value; }
        }
        public TreeNode[] constructTree()
        {
            try
            {
                if (srcNodes == null)
                {
                    srcNodes = new ArrayList();
                    srcNodes = loadTreeFromDB();// getTreeData();

                }

                TreeNode[] Parents = new TreeNode[srcNodes.Count];
                 // source
                Node tmpNode;
                leave tmpLeave;
                TreeNode[] sub;
                TreeNode tmpParentNode;
                TreeNode tmpLeaveNode;
                for (int i = 0; i < srcNodes.Count; i++)
                {

                    tmpNode = (Node)srcNodes[i];
                    
                    sub = new TreeNode[tmpNode.Leaves.Count];
                    for (int child = 0; child < tmpNode.Leaves.Count; child++)
                    {
                        tmpLeave = (leave)tmpNode.Leaves[child];
                        tmpLeaveNode=new TreeNode(tmpLeave.ToString());
                        tmpLeaveNode.Tag=tmpLeave.ID;
                        sub[child] =tmpLeaveNode ;
                       

                    }
                    tmpParentNode = new TreeNode(tmpNode.ToString(), sub);
                    tmpParentNode.Tag = tmpNode.ParentID;
                    Parents[i] = tmpParentNode;
                    
                    
                }

                return Parents;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("constructTree Error " + e.Message);
                return null;
            }
        }
        public static bool flushTreeToDB(string caption)
        {
            try
            {
                ArrayList param = new ArrayList();
                param.Add(new MySqlParameter("@val", caption));
                MYSQL.MYSQLExecuteProcedure("Collection_Add", MYSQL.getConnection(), param).Close();
                srcNodes = null;


                return true;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Flush tree to DB  Error " + e.Message);
                return false;

            }
        }

        public static bool isParentExist(string parentCaption)
        {
            Node tmp;
            if (SrcNodes == null || SrcNodes.Count == 0)
                return false;
            try
            {
                for (int i = 0; i < SrcNodes.Count; i++)
                {
                    tmp = new Node();
                    tmp = (Node)SrcNodes[i];
                    if (tmp.ParentCaption.ToLower() == parentCaption.ToLower())
                    {
                        return true;

                    }
                }
            }
            catch
            {
            }
            return false;
        }

        private ArrayList loadTreeFromDB()
        {
            try
            {
                ArrayList param = new ArrayList();
                 MySqlDataReader rdr;
                 rdr = MYSQL.MYSQLExecuteProcedure("Collection_loadAllItems", MYSQL.getConnection(), param);
                  
                int count=0;
                string currentParnet="";
                string PrevParnet="";

                int currentCOllID = -1;
                int prevCOllID = -1;

                ArrayList leaves = new ArrayList();
                  leave tmpLeave = new leave();
                  Node parentNode = new Node();

                ArrayList allParent= new ArrayList();
                Hashtable Htb = new Hashtable();
                bool hasItem = false;
                  while (rdr.Read())
                  {
                   
                      currentParnet=rdr["Coll_Name"].ToString();
                      currentCOllID = int.Parse(rdr["Coll_ID"].ToString());
                      if (count==0){
                          PrevParnet=currentParnet;
                          prevCOllID = currentCOllID;
                          Htb.Add(currentParnet, currentParnet);
                      }

                      if (Htb.ContainsKey(currentParnet)){
                          // accumulate
                          try
                          {
                              tmpLeave.ID = int.Parse(rdr["Item_ID"].ToString());
                              hasItem = true;
                              
                          }
                          catch { hasItem = false; }
                          try
                          {
                              tmpLeave.Value = rdr["Title"].ToString();
                          }
                          catch { hasItem = false; }
                          if (hasItem)
                          {
                              leaves.Add(tmpLeave);
                          }
                        tmpLeave = new leave();

                      }else{
                           // pervious parent + the leaves are one object
                          parentNode.ParentCaption=PrevParnet;
                          parentNode.ParentID = prevCOllID;
                          parentNode.Leaves=leaves;
                          allParent.Add(parentNode);

                          parentNode = new Node();
                          tmpLeave = new leave();
                          leaves= new ArrayList();


                          currentParnet = rdr["Coll_Name"].ToString();
                          currentCOllID = int.Parse(rdr["Coll_ID"].ToString());
                          parentNode.ParentCaption = currentParnet;
                          parentNode.ParentID = currentCOllID;

                          try
                          {
                              tmpLeave.ID = int.Parse(rdr["Item_ID"].ToString());
                              hasItem = true;

                          }
                          catch { hasItem = false; }
                          try
                          {
                              tmpLeave.Value = rdr["Title"].ToString();
                            
                          }
                          catch { hasItem = false; }
                          if (hasItem)
                          {
                              leaves.Add(tmpLeave);
                          }
                          tmpLeave = new leave();

                          PrevParnet=currentParnet;
                          prevCOllID = currentCOllID;


                          Htb.Add(currentParnet,currentParnet);
                      }

                     
                      count++;
                          
                  }
                  parentNode.ParentCaption = PrevParnet;
                  parentNode.ParentID = prevCOllID;
 
                parentNode.Leaves = leaves;
                  allParent.Add(parentNode);



                rdr.Close();
                return allParent;

            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("loadTreeFromDB  Error " + e.Message);
                return null;
            }
        }
        private ArrayList getTreeData()
        {
            try
            {
                ArrayList ar = new ArrayList();
                ArrayList leaves = new ArrayList();
                Node tmp = new Node();
                leave tmpLeave = new leave();
                tmp.ParentID = 1;
                tmp.ParentCaption = "Parent 1";
                
                tmpLeave.ID = 1;
                tmpLeave.Value = "item 1";
                leaves.Add(tmpLeave);
                tmpLeave = new leave();

                tmpLeave.ID = 2;
                tmpLeave.Value = "item 2";
                leaves.Add(tmpLeave);
                tmpLeave = new leave();

                tmpLeave.ID = 3;
                tmpLeave.Value = "item 3";
                leaves.Add(tmpLeave);
                tmpLeave = new leave();

                tmpLeave.ID = 4;
                tmpLeave.Value = "item 4";
                leaves.Add(tmpLeave);
                tmpLeave = new leave();

                tmp.Leaves = new ArrayList( leaves);
                leaves.Clear();
                ar.Add(tmp);
                tmp = new Node();

                // 2nd node
                tmp.ParentID = 2;
                tmp.ParentCaption = "Parent 2";
                
                tmpLeave.ID = 5;
                tmpLeave.Value = "item 5";
                leaves.Add(tmpLeave);
                tmpLeave = new leave();

                tmp.Leaves = new ArrayList(leaves);
                leaves.Clear();
                ar.Add(tmp);
                tmp = new Node();

                // 3rd
                tmp.ParentID = 3;
                tmp.ParentCaption = "Parent 3";
                tmp.Leaves = new ArrayList(leaves);
                leaves.Clear();
                ar.Add(tmp);
                tmp = new Node();

                return ar;



               

            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("getTreeData Error " + e.Message);
                return null;
            }

        }


    }

    class leave
    {
        int id;
        string caption;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Value
        {
            get { return caption; }
            set { caption = value; }
        }

        public override string ToString()
        {
            return this.Value;
        }



    }
    class Node
    {
        int parent;
        string parentCaption;
        ArrayList leaves;
     
        public int ParentID
        {
            get { return parent; }
            set { parent = value; }
        }

        public string ParentCaption
        {
            get { return parentCaption; }
            set { parentCaption = value; }
        }

        public ArrayList Leaves
        {
            get { return leaves; }
            set { leaves = value; }
        }
        public override string ToString()
        {
            return this.ParentCaption;
        }


    }
}
