﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Resources;
using System.Globalization;

namespace DCDataEntry
{
    public partial class Human : Form
    {
        string humanName = "";
        Person p = null;
         string selectValue = "";
        public Human(string humaneName,string locale, RightToLeft RTL, bool rtlLayout)
        {
            this.humanName = humaneName;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(locale);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(locale);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(DCTool));
            this.RightToLeft = RTL;
            this.RightToLeftLayout = rtlLayout;



            InitializeComponent();

        }
         public string show()
        {
            this.ShowDialog();
            return selectValue;
        }
        private void Human_Load(object sender, EventArgs e)
        {
            reloadHuman();
        }

        private void cancelHuman_Click(object sender, EventArgs e)
        {
            selectValue = this.humanName;
            this.Close();
        }

        private void saveHuman_Click(object sender, EventArgs e)
        {
             p.pName=pName.Text.Trim();
             if (p.pName == String.Empty)
             {
                 MessageBox.Show(Constants.EmptyPersonName);
                 return;
             }
            p.nationality=pNationality.Text;
            p.p_DOB=pDOB.Value ;
            p.P_VidNum = Decimal.ToInt32(pVidNum.Value);
            p.P_AudNum = Decimal.ToInt32(pAudNum.Value);
            p.P_Trip=pTrips.Text;
            p.P_History=pHistory.Text;
            if (p.save())
            {
                selectValue = p.pName;
                this.Close();
            }
        }

        private void reloadHuman()
        {
            try
            {
                clear();
                
                 p = new Person();
               p= p.loadPersonInfo(this.humanName);
                if (p==null || p.p_id > -1)
                {
                    pName.Text = p.pName;
                    pNationality.Text = p.nationality;
                    pNationality.Select();

                    pDOB.Value = p.p_DOB;
                    pVidNum.Value = p.P_VidNum;
                    label10.Text = p.vid_DB.ToString();
                    pAudNum.Value = p.P_AudNum;
                    label11.Text = p.auds_DB.ToString();
                    pTrips.Text = p.P_Trip;
                    pHistory.Text = p.P_History;

                }
                else
                {
                    MessageBox.Show(Constants.loadError);
                    cancelHuman_Click(null, null);
                    
                }


            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Error while loading human in human GUI : " + e.Message);
                MessageBox.Show(Constants.loadError);
                cancelHuman_Click(null, null);

            }
        }

        private void clear()
        {
            pName.Text = "";
            pNationality.Items.Clear();
            pNationality.Items.AddRange(DataLoader.loadCountry());
            pDOB.Value = DateTime.Today;
            pVidNum.Value = 0;
            label10.Text = "";
            pAudNum.Value = 0;
            label11.Text = "";
            pTrips.Text = "";
            pHistory.Text = "";

        }
        
     
    }
}
