﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using MySql.Data.MySqlClient;
namespace DCDataEntry
{
    class Person
    {
         public int p_id = -1;
         public string pName = "";
         public DateTime p_DOB;
         public  int P_VidNum;
         public  int P_AudNum;
         public  string P_Trip;
         public string P_History;
         public string nationality;
         public  int vid_DB;
         public  int auds_DB;
         public  Person()
        {
        
        }
      
        public  Person loadPersonInfo(string p_Name){
            MySqlDataReader dr = null;
            try{
                              
                
                    ArrayList param = new ArrayList();
                    param.Add(new MySqlParameter("@pname", p_Name));
                    dr = MYSQL.MYSQLExecuteProcedure("Narrator_LoadInfo", MYSQL.getConnection(), param);
                    while (dr.Read())
                    {
                        p_id = int.Parse(dr["P_ID"].ToString());
                        pName = dr["P_Name"].ToString();
                        try { 
                            p_DOB = DateTime.Parse(dr["P_DOB"].ToString()); 
                        }
                        catch
                        {
                            p_DOB = DateTime.MinValue;
                        }
                        P_VidNum = int.Parse(dr["P_VidNum"].ToString());
                        P_AudNum = int.Parse(dr["P_AudNum"].ToString());
                        P_Trip = dr["P_Trip"].ToString();
                        P_History = dr["P_History"].ToString();
                        nationality = dr["N_Caption"].ToString();
                        vid_DB = int.Parse(dr["vids"].ToString());
                        auds_DB = int.Parse(dr["auds"].ToString());
                    }

                    return this; 


            }catch(Exception e){
                LoggingWrapper.LoggingWrapper.Instance.Error("Error while loading Person info "+pName+ " "+e.Message );
                return null;
            }
            finally
            {
                try{
                    dr.Close();
                }catch{}

            }
        }

        public bool save()
        {
            try
            {
                if (isPNameExist())
                {
                    System.Windows.Forms.MessageBox.Show(Constants.PersonExists);
                    return false;
                }
                ArrayList param = new ArrayList();
                //new MySqlParameter("@role", 1)
                param.Add(new MySqlParameter("@pid", p_id));
                param.Add(new MySqlParameter("@pname", pName));
                param.Add(new MySqlParameter("@pdob", p_DOB));
                param.Add(new MySqlParameter("@vidNum", P_VidNum));
                param.Add(new MySqlParameter("@audNum", P_AudNum));
                param.Add(new MySqlParameter("@ptrip", P_Trip));
                param.Add(new MySqlParameter("@phistory", P_History));
                param.Add(new MySqlParameter("@country", nationality));
                MYSQL.MYSQLExecuteProcedure("Narrator_Update", MYSQL.getConnection(), param).Close();


                return true;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Error while Saving Person info " + pName + " " + e.Message);
                return false;

            }
        }

        public bool isPNameExist()
        {
           MySqlDataReader dr = null;
           bool itemExists = false;
            try
            {
                ArrayList param = new ArrayList();
                //new MySqlParameter("@role", 1)
                param.Add(new MySqlParameter("@pname", pName));
                param.Add(new MySqlParameter("@pid", p_id));
                dr =MYSQL.MYSQLExecuteProcedure("Person_checkName", MYSQL.getConnection(), param);
                
               while (dr.Read())
               {
                   itemExists = true;
               }

             

            
            }
            catch (Exception e)
            {
                itemExists = false;
                
            }
            dr.Close();
            return itemExists;
        }
    }
}
