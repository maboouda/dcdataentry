﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using System.Resources;
using System.Reflection;
using System.IO;
using System.Diagnostics;


namespace DCDataEntry
{
    public partial class DCTool : Form
    {
        int bIndex = 0;
        int contributorIndex = 0;
        int locationIndex = 0;
        string locale = "en-US";
        RightToLeft RTL = RightToLeft.No;
        bool rtlLayout = false;
        Item currentItem;
        ToolTip toolTip1 = new ToolTip();
         

        public DCTool()
        {
            InitializeComponent();
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 1000;
            toolTip1.ReshowDelay = 500;
            toolTip1.ShowAlways = true;
            toolTip1.SetToolTip(label2, Constants.TitleHint);
            toolTip1.SetToolTip(TitleTB, Constants.TitleHint);
            toolTip1.SetToolTip(dateTimePicker2, Constants.creationDateHint);
            toolTip1.SetToolTip(label5, Constants.creationDateHint);
            toolTip1.SetToolTip(dateTimePicker1, Constants.pubDateHint);
            toolTip1.SetToolTip(label4, Constants.pubDateHint);
            toolTip1.SetToolTip(CreatorDDL, Constants.mainContributorHint);
            toolTip1.SetToolTip(label3, Constants.mainContributorHint);

            toolTip1.SetToolTip(cprDDL, Constants.copyrightHint);
            toolTip1.SetToolTip(label11, Constants.copyrightHint);

            toolTip1.SetToolTip(mainLocationCB, Constants.mainLocationHint);
            toolTip1.SetToolTip(label10, Constants.mainLocationHint);

            toolTip1.SetToolTip(narratorCB, Constants.narratorHint);
            toolTip1.SetToolTip(label9, Constants.narratorHint);
            
        }

        #region Load 


        private void DCTool_Load(object sender, EventArgs e)
        {

            clearControls();
            //select language 
            langCB.SelectedIndex = 0;
            langCB.Select();

            // load tree
            //loadTree();
           // loadAllDDL();      



        }

        private void loadAllDDL()
        {
         
            setPanel(contributorPanel, contributorIndex, DataLoader.loadContributor, DataLoader.DataSource.Contributor, Constants.contributorLabel, null,Constants.ContributorsHint);
            setPanel(publisherPanel, locationIndex, DataLoader.loadPublisher, DataLoader.DataSource.Publisher, Constants.publisherLabel, null,Constants.publisherHint);
            setPanel(generPanel, locationIndex, DataLoader.loadGener, DataLoader.DataSource.Gener, Constants.generLabel, null, Constants.generHint);
            setPanel(subjectPanel, locationIndex, DataLoader.loadSubject, DataLoader.DataSource.Subject, Constants.subjectLabel, null, Constants.subjectHint);
            setPanel(TimePersiodPanel, locationIndex, DataLoader.loadTimePeriod, DataLoader.DataSource.TimePeriod, Constants.timePeriodLabel, null, Constants.timePeriodHint);
            setPanel(locationPanel, locationIndex, DataLoader.loadLocation, DataLoader.DataSource.Location, Constants.locationLabel, null, Constants.locationHint);
            setPanel(personPanel, locationIndex, DataLoader.loadPerson, DataLoader.DataSource.Person, Constants.personsLabel, null, Constants.personHint);

        }

        private void setPanel(FlowLayoutPanel flp, int index,Func<string[]> dataRetrieveMethod, DataLoader.DataSource type, string labelName, string[] setList, string hint)
        {
            try
            {

                flp.Controls.Clear();

                runTimeControlAdder rT = new runTimeControlAdder(flp, index, dataRetrieveMethod, type, labelName, hint);
                rT.loadPanel();
             
               
                if (setList != null && setList.Length > 0) {

                    for (int i = 1; i < setList.Length; i++)
                    {
                        rT.cbADD_Click(this, null);

                    }

                    int count = 0;
                    foreach (Panel p in flp.Controls.OfType<Panel>())
                    {
                        foreach (ComboBox cb in p.Controls.OfType<ComboBox>())
                        {
                            cb.Text = setList[count];
                            count++;

                        }
                    }
            }
            }
            catch
            {

            }
        }
        #endregion
        #region Language
        /**
         * 
         *  ==========================================Language Start===========================================
         */
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

             locale = "en-US";
            RTL = RightToLeft.No;
            rtlLayout = false;

            if (langCB.SelectedIndex == 1)
            {
                //Arabic language selected
                locale = "ar-EG";
                RTL = RightToLeft.Yes;
                rtlLayout = true;

            }

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(locale);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(locale);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(DCTool));
            this.RightToLeft = RTL;
            this.RightToLeftLayout = rtlLayout;

            resources.ApplyResources(this, "$this");
            applyResources(resources, this.Controls);


        }

        private void applyResources(ComponentResourceManager resources, Control.ControlCollection ctls)
        {
            foreach (Control ctl in ctls)
            {
                resources.ApplyResources(ctl, ctl.Name);
                applyResources(resources, ctl.Controls);
            }
        }

        #endregion
        #region Tree
        /**
         * 
         *  ==========================================Tree===========================================
         */

        private void loadTree()
        {
            treeView1.Nodes.Clear();
            TreeLoader.SrcNodes = null;
            TreeLoader tl = new TreeLoader();
            TreeNode[] nodes = tl.constructTree();
            if (nodes == null)
                throw new Exception("Error in Tree construction");
            for (int i = 0; i < nodes.Length; i++) {
                treeView1.Nodes.Add(nodes[i]);
            }
            treeView1.ExpandAll();
        }


        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                TreeNode node = treeView1.SelectedNode;
                if (node.Parent != null)
                {
                    // get item id
                    int itemID = int.Parse(treeView1.SelectedNode.Tag.ToString());
                    currentItem = new Item();
                    currentItem = currentItem.loadItem(itemID);
                    renderItem(currentItem);
                    // enable export button

                    exportButton.Enabled = true;

                }
                else
                {
                    exportButton.Enabled = false;
                }
                /* TreeNode node = treeView1.SelectedNode;
                if (node.Parent == null)
                {
                    MessageBox.Show("Parent Node : " + treeView1.SelectedNode.Tag);
                }
                else
                {

                    MessageBox.Show("Child node: " + treeView1.SelectedNode.Text + " : " + treeView1.SelectedNode.FullPath);

                }*/

                // LoadItem();
            }
            catch (Exception e1)
            {
                MessageBox.Show(Constants.loadError);
                LoggingWrapper.LoggingWrapper.Instance.Error("treeView1_AfterSelect load error :" + e1.Message);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //treeView1.Nodes.Clear();
            //TreeLoader.SrcNodes.Add()
            Node parent = new Node();
            if (textBox4.Text.Trim() == String.Empty)
            {
                MessageBox.Show(Constants.EmptyCollection);
                return;
            }
            if (TreeLoader.isParentExist(textBox4.Text.Trim()))
            {
                MessageBox.Show(Constants.ParentAlreadyExists);
                return;
            }

            parent.ParentCaption = textBox4.Text.Trim();
            parent.Leaves = new System.Collections.ArrayList();
            TreeLoader.SrcNodes.Add(parent);
            TreeLoader.flushTreeToDB(parent.ParentCaption);
            textBox4.Text = "";
            loadTree();

        }

        #endregion
        #region File Section
        /**
       * 
       *  ==========================================Files Section===========================================
       */
        private void button1_Click(object sender, EventArgs e)
        {
            string path = null;
            if ((path = openFileChoser()) != null)
            {
                textBox1.Text = path;
            }

        }

        private void Play1_Click(object sender, EventArgs e)
        {
            Process.Start(textBox1.Text.Trim());

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string path = null;
            if ((path = openFileChoser()) != null)
            {
                textBox2.Text = path;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string path = null;
            if ((path = openFileChoser()) != null)
            {
                textBox3.Text = path;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process.Start(textBox2.Text.Trim());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Process.Start(textBox3.Text.Trim());
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            filePathChecker(textBox1, Play1, config1, warningLBL);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            filePathChecker(textBox2, Play2, config2, Warn2);

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

            filePathChecker(textBox3, Play3, config3, Warn3);
        }



        private string openFileChoser()
        {
            string path = null;
            OpenFileDialog file = new OpenFileDialog();
            if (file.ShowDialog() == DialogResult.OK)
            {
                path = file.FileName;

            }
            return path;

        }

        private void filePathChecker(TextBox toCheck, Button play, Button config, Label warn)
        {
            try
            {

                if (new FileInfo(toCheck.Text.Trim()).Exists)
                {
                    play.Visible = true;
                    config.Visible = true;
                    warn.Visible = false;
                }
                else
                {
                    play.Visible = false;
                    config.Visible = false;
                    warn.Visible = true;

                }
            }
            catch
            {
                play.Visible = false;
                config.Visible = false;
                warn.Visible = false;

            }
        }

        private string[] getFiles()
        {
            System.Collections.ArrayList a = new System.Collections.ArrayList();
            if (textBox1.Text.Trim() != String.Empty)
            {
                a.Add(textBox1.Text.Trim() + "%" + GetMimeType(textBox1.Text.Trim()));
            }
            if ((textBox2.Text.Trim() != String.Empty) && (!a.Contains(textBox2.Text.Trim())))
            {
                a.Add(textBox2.Text.Trim() + "%" + GetMimeType(textBox2.Text.Trim()));
            }
            if ((textBox3.Text.Trim() != String.Empty) && (!a.Contains(textBox3.Text.Trim())))
            {
                a.Add(textBox3.Text.Trim() + "%" + GetMimeType(textBox3.Text.Trim()));
            }
            return a.ToArray(typeof(string)) as string[];
        }
        private static string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType.Substring(0, mimeType.IndexOf("/"));
        }

        #endregion
        #region Creator, Narrator and Main Location
        /**
       * 
       *  ==========================================Creator, Narrator and Main Location===========================================
       */
        private void CreatorDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            addNewCB((ComboBox)sender, DataLoader.DataSource.Creator);
        }
        private void mainLocationCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            addNewCB((ComboBox)sender, DataLoader.DataSource.Location);
        }

        private void narratorCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            addNewCB((ComboBox)sender, DataLoader.DataSource.Narrator);
        }


        private void addNewCB(ComboBox current, DataLoader.DataSource src)
        {

            if (current.SelectedIndex == 1)
            {
                // add new item
                DataEntry de = new DataEntry();
                de.ShowDialog();
                // add to comboBox
                if (de.Caption != String.Empty)
                {
                    int result = current.FindStringExact(de.Caption);
                    if (result > -1)
                    {
                        MessageBox.Show(Constants.valueAlreadyExists);
                        current.SelectedIndex = result;
                    }
                    else
                    {
                        if (DataLoader.addItem(src, de.Caption) == null)
                        {
                            MessageBox.Show(Constants.addederror);
                            return;
                        }

                        current.Items.Add(de.Caption);
                        current.Text = de.Caption;
                    }

                }


            }
        }

        private void narratorB_Click(object sender, EventArgs e)
        {
            Human h = new Human(narratorCB.Text, locale, RTL, rtlLayout);
            string tmp = h.show();
            // reload narrator
            try
            {
                narratorCB.Items.Clear();
                narratorCB.Items.AddRange(DataLoader.loadNarrator());
                narratorCB.Text = tmp;
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }

        }
        #endregion
        #region Save, clear and loadItem

        /**
        * 
        *  ==========================================Save , clear and load===========================================
        */

        private void  renderItem(Item item)
        {
            try
            {
                setPanel(contributorPanel, contributorIndex, DataLoader.loadContributor, DataLoader.DataSource.Contributor, Constants.contributorLabel, item.Contributor, Constants.ContributorsHint);
                setPanel(publisherPanel, locationIndex, DataLoader.loadPublisher, DataLoader.DataSource.Publisher, Constants.publisherLabel, item.Publisher, Constants.publisherHint);
                setPanel(generPanel, locationIndex, DataLoader.loadGener, DataLoader.DataSource.Gener, Constants.generLabel, item.Gener, Constants.generHint);
                setPanel(subjectPanel, locationIndex, DataLoader.loadSubject, DataLoader.DataSource.Subject, Constants.subjectLabel, item.Subject, Constants.subjectHint);
                setPanel(TimePersiodPanel, locationIndex, DataLoader.loadTimePeriod, DataLoader.DataSource.TimePeriod, Constants.timePeriodLabel, item.Timeperiod, Constants.timePeriodHint);
                setPanel(locationPanel, locationIndex, DataLoader.loadLocation, DataLoader.DataSource.Location, Constants.locationLabel, item.Location, Constants.locationHint);
                setPanel(personPanel, locationIndex, DataLoader.loadPerson, DataLoader.DataSource.Person, Constants.personsLabel, item.Person,Constants.personHint);

                textBox1.Text = (item.Files == null || item.Files.Length < 1 || item.Files[0] == null) ? "" : item.Files[0];
                textBox2.Text = (item.Files == null || item.Files.Length<2 || item.Files[1] == null) ? "" : item.Files[1];
                textBox3.Text = (item.Files == null || item.Files.Length < 3 || item.Files[2] == null) ? "" : item.Files[2];
                sumTextBox.Text = item.Summary;
                TitleTB.Text = item.Title;
                CreatorDDL.Text = item.Creator == null ? "" : item.Creator;
                narratorCB.Text = item.Narrator == null ? "" : item.Narrator;
                mainLocationCB.Text = item.MainLocation == null ? "" : item.MainLocation;
                dateTimePicker1.Value = item.PublishDate;
                dateTimePicker2.Value = item.CreationDate;
                cprDDL.Text = item.Copyright == null ? "" : item.Copyright;
               
               
             
                   
                

            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Item rendering error :" + e.Message);
                MessageBox.Show(Constants.loadError);
            }
        }

        

        private void saveButton_Click(object sender, EventArgs e)
        {
            Item item=collectData();
            if (currentItem == null)
            {
                item.ItemID = -1;

            }
            else
            {
                item.ItemID = currentItem.ItemID;

            }
            // validate
            if (validate(item))
            {
                if (item.updateDB())
                {
                    MessageBox.Show(Constants.addedSucc);
                    clearControls();
                }
                else
                {
                    MessageBox.Show(Constants.addederror);


                }
            }

         

        }
        private int getSelectedItemID()
        {
            int itemID = -1;
            try
            {
                // check if item is new or old
                TreeNode node = treeView1.SelectedNode;
               
                if (node != null && node.Parent != null)
                {


                    try
                    {
                        itemID = int.Parse(node.Tag.ToString());
                    }
                    catch (Exception ee)
                    {
                        itemID = -1;
                    }
                    
                }
            }
            catch {
                itemID = -1;
            }

            return itemID;
        }

        private void clear_Click(object sender, EventArgs e)
        {
            currentItem = new Item();
            clearControls();
        }
        private Item collectData()
        {
            Item item = new Item();
          
            item.Contributor = readFLowLayOut(contributorPanel);
            item.Publisher = readFLowLayOut(publisherPanel);
            item.Gener = readFLowLayOut(generPanel);
            item.Subject = readFLowLayOut(subjectPanel);
            item.Timeperiod = readFLowLayOut(TimePersiodPanel);
            item.Location = readFLowLayOut(locationPanel);
            item.Person = readFLowLayOut(personPanel);
            item.Title = TitleTB.Text.Trim();
            item.Creator = CreatorDDL.Text;
            item.Summary = sumTextBox.Text.Trim();
            item.PublishDate = dateTimePicker1.Value;
            item.CreationDate = dateTimePicker2.Value;
            item.Narrator = narratorCB.Text;
            item.MainLocation = mainLocationCB.Text;
            item.Copyright = cprDDL.Text;
          


            try
            {
                item.Collection = treeView1.SelectedNode.Text;
                item.CollID = int.Parse(treeView1.SelectedNode.Tag.ToString());
            }
            catch
            {
                item.Collection = null;
                item.CollID = -1;
            }
             item.Files = getFiles();

             return item;
        }
    
        private void clearControls()
        {
            // media
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            sumTextBox.Text = "";
            TitleTB.Text = "";
            CreatorDDL.Items.Clear();
            CreatorDDL.Items.AddRange(DataLoader.loadCreator());

            narratorCB.Items.Clear();
            narratorCB.Items.AddRange(DataLoader.loadNarrator());

            mainLocationCB.Items.Clear();
            mainLocationCB.Items.AddRange(DataLoader.loadLocation());
            
            cprDDL.Items.Clear();
            cprDDL.Items.AddRange(DataLoader.loadCopyright());

            dateTimePicker1.Value = DateTime.Today;
            dateTimePicker2.Value = DateTime.Today;
            exportButton.Enabled = false;

            loadAllDDL();
            treeView1.Nodes.Clear();
            loadTree();


        }

        #endregion
        #region Validation
        /**
      * 
      *  ==========================================Validation===========================================
      */
        private bool validate(Item item)
        {
            try
            {
               string msg;

               if (item.Collection == null || item.Collection.Trim() == String.Empty)
               {
                   msg = Constants.selectCollection;
                   throw new Exception(msg);
               }

                // file
               if (item.Files == null || item.Files.Length == 0)
               {
                   msg = Constants.EmptyFile;
                   throw new Exception(msg);
               }
               else
               {
                   for (int i = 0; i < item.Files.Length; i++)
                   {
                       string location = item.Files[i];
                       location = location.Substring(0, location.LastIndexOf("%"));
                       FileInfo tmpFile =new FileInfo(location);
                       if (!(tmpFile.Exists))
                       {
                           msg = Constants.incorrectFile;
                           throw new Exception(msg);

                       }
                   }
               }

                // title
               if (item.Title.Trim() == String.Empty)
               {
                   msg = Constants.EmptyTitle;
                   throw new Exception(msg);
               }
               if (item.Summary.Trim() == String.Empty)
               {
                   msg = Constants.EmptySum;
                   throw new Exception(msg);
               }
               if (item.Creator.Trim() == String.Empty)
               {
                   msg = Constants.EmptyCreator;
                   throw new Exception(msg);
               }

               if (item.Narrator.Trim() == String.Empty)
               {
                   msg = Constants.EmptyNarrator;
                   throw new Exception(msg);
               }

               if (item.MainLocation.Trim() == String.Empty)
               {
                   msg = Constants.EmptyMainLocation;
                   throw new Exception(msg);
               }

               if (item.Copyright.Trim() == String.Empty)
               {
                   msg = Constants.EmptyCopyright;
                   throw new Exception(msg);
               }
          
                // contributors
               if (item.Contributor == null || item.Contributor.Length == 0)
               {
                   msg = Constants.EmptyCont;
                   throw new Exception(msg);
               }

               if (treeView1.SelectedNode.Parent != null)
               {
                   msg = Constants.selectCollection;
                   throw new Exception(msg);


               }

             
               return true;
            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("Item validation error " + item.ToString());
                MessageBox.Show(e.Message);
                return false;
            }
        }


      #endregion
        #region readFLowLayOut

        private string[] readFLowLayOut(FlowLayoutPanel flp)
        {
            try
            {
                System.Collections.ArrayList ar = new System.Collections.ArrayList();
                foreach (Panel p in flp.Controls.OfType<Panel>())
                {
                    foreach (ComboBox cb in p.Controls.OfType<ComboBox>()) 
                    {
                        if((cb.Text.Trim()!=String.Empty) && (cb.Text!=Constants.addNew) && (!ar.Contains(cb.Text))){
                            
                            ar.Add(cb.Text);
                        }
                    }
                }
                return ar.ToArray(typeof(string)) as string[];

            }
            catch (Exception e)
            {
                LoggingWrapper.LoggingWrapper.Instance.Error("readFLowLayOut error " + flp.Name+ " " + e.Message);
                return null;
            }
        }


        #endregion

        private void button4_Click_1(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = treeView1.SelectedNode;
                if (node!=null&&node.Parent != null)
                {
                  
                 int itemID=-1;
                    try{
                      itemID= int.Parse(node.Tag.ToString());
                   }catch(Exception ee){
                       
                   }
                    if (itemID==-1){
                        MessageBox.Show(Constants.ItemIsNotSavedYet);
                        return;
                    }

                    DialogResult dr = MessageBox.Show(Constants.Confirmation, "Delete", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        Item i = new Item();
                        if (i.deleteItem(itemID))
                            clearControls();
                        else
                        {
                            MessageBox.Show(Constants.DeleteError);
                            return;
                        }
                    }

                }
                else
                {
                    MessageBox.Show(Constants.selectItem);
                    return;
                }

            }catch(Exception e1)
            {
                MessageBox.Show(Constants.DeleteError + " " + e1.Message); ;
                LoggingWrapper.LoggingWrapper.Instance.Error("Error while deleting item " + e1.Message);
            }
        }

        private void config1_Click(object sender, EventArgs e)
        {
            string mime = GetMimeType(textBox1.Text.Trim());
            if (mime == "video" || mime == "audio")
            {
                MediaPlayer mp = new MediaPlayer(textBox1.Text.Trim());
                mp.ShowDialog();
            }
            else
            {
                MessageBox.Show(Constants.FormatNotSupported);
                return;
            }
            
           
        }

        private void config2_Click(object sender, EventArgs e)
        {
            string mime = GetMimeType(textBox2.Text.Trim());
            if (mime == "video" || mime == "audio")
            {
                MediaPlayer mp = new MediaPlayer(textBox2.Text.Trim());
                mp.ShowDialog();
            }
            else
            {
                MessageBox.Show(Constants.FormatNotSupported);
                return;
            }
        }

        private void config3_Click(object sender, EventArgs e)
        {
            string mime = GetMimeType(textBox3.Text.Trim());
            if (mime == "video" || mime == "audio")
            {
                MediaPlayer mp = new MediaPlayer(textBox3.Text.Trim());
                mp.ShowDialog();
            }
            else
            {
                MessageBox.Show(Constants.FormatNotSupported);
                return;
            }
        }

        private void TraverseRecursive(TreeNode treeNode, string path)
        {
            // Print the node.
         //   System.Diagnostics.Debug.WriteLine(treeNode.Text);
         //   MessageBox.Show(treeNode.Text);
            // Print each node recursively.
            int itemID = -1;
            Item cItem = null;
            string collName = treeNode.Text;
            foreach (TreeNode tn in treeNode.Nodes)
            {
                //TraverseRecursive(tn);
                // get item id
                 itemID = int.Parse(tn.Tag.ToString());
                 cItem = new Item();
                 cItem = cItem.loadItem(itemID);
                 ModsObject mo = new ModsObject(cItem);
                 mo.ItemDir = path + "\\MOC\\"+collName+"\\Item" + itemID;
                 mo.createDirectories();
                 ModsWriter mw = new ModsWriter();
                 ThreadStart ts = delegate { mw.descMDBuilder(mo, DateTime.Now.ToString().Replace(':', '_').Replace('/', '_').Replace(' ', '_') + "_mods.xml",false); };
                 Thread t = new Thread(ts);
                 t.Start();



            }
        }



        private void exportButton_Click(object sender, EventArgs e)
        {

       
            TreeNode node = treeView1.SelectedNode;

            if (node.Parent != null)
            {
                // get item id
                int itemID = int.Parse(treeView1.SelectedNode.Tag.ToString());
                currentItem = new Item();
                currentItem = currentItem.loadItem(itemID);

                string path = null;
                // open file location
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                DialogResult result = folderBrowserDialog1.ShowDialog();
                if( result == DialogResult.OK )
                {

                    ModsObject mo = new ModsObject(currentItem);
                    mo.ItemDir = folderBrowserDialog1.SelectedPath + "\\MOC\\Item" + itemID;
                    mo.createDirectories();
                    ModsWriter mw = new ModsWriter();
                    ThreadStart ts = delegate { mw.descMDBuilder(mo, DateTime.Now.ToString().Replace(':', '_').Replace('/', '_').Replace(' ', '_') + "_mods.xml",true); };
                    Thread t = new Thread(ts);
                    t.Start();
                                       
                }

                           

            }
            else
            {
                MessageBox.Show(Constants.selectItem);
            }
        }

        private void exportAll_Click(object sender, EventArgs e)
        {

            // open file location
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            string path;
            if (result == DialogResult.OK)
            {
                path = folderBrowserDialog1.SelectedPath;

                 TreeNodeCollection nodes = treeView1.Nodes;
                foreach (TreeNode n in nodes)
                {
                    TraverseRecursive(n,  path);
                }
            }

            MessageBox.Show(Constants.SaveIsDone);

        }


       

       

      

     
      



    }
}