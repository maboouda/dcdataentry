﻿namespace DCDataEntry
{
    partial class MediaPlayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MediaPlayer));
            this.Player = new AxWMPLib.AxWindowsMediaPlayer();
            this.vidFrameGrid = new System.Windows.Forms.DataGridView();
            this.captionTB = new System.Windows.Forms.TextBox();
            this.frameNumber = new System.Windows.Forms.NumericUpDown();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Player)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vidFrameGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frameNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // Player
            // 
            this.Player.Enabled = true;
            this.Player.Location = new System.Drawing.Point(12, 12);
            this.Player.Name = "Player";
            this.Player.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Player.OcxState")));
            this.Player.Size = new System.Drawing.Size(405, 271);
            this.Player.TabIndex = 1;
            this.Player.MediaError += new AxWMPLib._WMPOCXEvents_MediaErrorEventHandler(this.Player_MediaError);
            // 
            // vidFrameGrid
            // 
            this.vidFrameGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vidFrameGrid.Location = new System.Drawing.Point(14, 348);
            this.vidFrameGrid.Name = "vidFrameGrid";
            this.vidFrameGrid.Size = new System.Drawing.Size(417, 181);
            this.vidFrameGrid.TabIndex = 4;
            this.vidFrameGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.vidFrameGrid_CellEndEdit);
            // 
            // captionTB
            // 
            this.captionTB.Location = new System.Drawing.Point(79, 300);
            this.captionTB.Multiline = true;
            this.captionTB.Name = "captionTB";
            this.captionTB.Size = new System.Drawing.Size(312, 42);
            this.captionTB.TabIndex = 5;
            // 
            // frameNumber
            // 
            this.frameNumber.Location = new System.Drawing.Point(12, 300);
            this.frameNumber.Name = "frameNumber";
            this.frameNumber.Size = new System.Drawing.Size(50, 20);
            this.frameNumber.TabIndex = 6;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(397, 300);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(34, 41);
            this.button4.TabIndex = 7;
            this.button4.Text = "Add";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(458, 506);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(458, 477);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // MediaPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(547, 541);
            this.ControlBox = false;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.frameNumber);
            this.Controls.Add(this.captionTB);
            this.Controls.Add(this.vidFrameGrid);
            this.Controls.Add(this.Player);
            this.Controls.Add(this.button1);
            this.Name = "MediaPlayer";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MediaPlayer";
            ((System.ComponentModel.ISupportInitialize)(this.Player)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vidFrameGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frameNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxWMPLib.AxWindowsMediaPlayer Player;
        private System.Windows.Forms.DataGridView vidFrameGrid;
        private System.Windows.Forms.TextBox captionTB;
        private System.Windows.Forms.NumericUpDown frameNumber;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}