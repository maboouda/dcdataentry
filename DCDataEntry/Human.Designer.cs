﻿namespace DCDataEntry
{
    partial class Human
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Human));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pName = new System.Windows.Forms.TextBox();
            this.pNationality = new System.Windows.Forms.ComboBox();
            this.pVidNum = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.pAudNum = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.pTrips = new System.Windows.Forms.TextBox();
            this.pHistory = new System.Windows.Forms.TextBox();
            this.pDOB = new System.Windows.Forms.DateTimePicker();
            this.saveHuman = new System.Windows.Forms.Button();
            this.cancelHuman = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pVidNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAudNum)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // pName
            // 
            resources.ApplyResources(this.pName, "pName");
            this.pName.Name = "pName";
            // 
            // pNationality
            // 
            this.pNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pNationality.FormattingEnabled = true;
            resources.ApplyResources(this.pNationality, "pNationality");
            this.pNationality.Name = "pNationality";
            // 
            // pVidNum
            // 
            resources.ApplyResources(this.pVidNum, "pVidNum");
            this.pVidNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.pVidNum.Name = "pVidNum";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // pAudNum
            // 
            resources.ApplyResources(this.pAudNum, "pAudNum");
            this.pAudNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.pAudNum.Name = "pAudNum";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // pTrips
            // 
            resources.ApplyResources(this.pTrips, "pTrips");
            this.pTrips.Name = "pTrips";
            // 
            // pHistory
            // 
            resources.ApplyResources(this.pHistory, "pHistory");
            this.pHistory.Name = "pHistory";
            // 
            // pDOB
            // 
            resources.ApplyResources(this.pDOB, "pDOB");
            this.pDOB.Name = "pDOB";
            // 
            // saveHuman
            // 
            resources.ApplyResources(this.saveHuman, "saveHuman");
            this.saveHuman.Name = "saveHuman";
            this.saveHuman.UseVisualStyleBackColor = true;
            this.saveHuman.Click += new System.EventHandler(this.saveHuman_Click);
            // 
            // cancelHuman
            // 
            resources.ApplyResources(this.cancelHuman, "cancelHuman");
            this.cancelHuman.Name = "cancelHuman";
            this.cancelHuman.UseVisualStyleBackColor = true;
            this.cancelHuman.Click += new System.EventHandler(this.cancelHuman_Click);
            // 
            // Human
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add(this.cancelHuman);
            this.Controls.Add(this.saveHuman);
            this.Controls.Add(this.pDOB);
            this.Controls.Add(this.pHistory);
            this.Controls.Add(this.pTrips);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pAudNum);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pVidNum);
            this.Controls.Add(this.pNationality);
            this.Controls.Add(this.pName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Human";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.Human_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pVidNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAudNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox pName;
        private System.Windows.Forms.ComboBox pNationality;
        private System.Windows.Forms.NumericUpDown pVidNum;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown pAudNum;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox pHistory;
        private System.Windows.Forms.DateTimePicker pDOB;
        private System.Windows.Forms.Button saveHuman;
        private System.Windows.Forms.Button cancelHuman;
        private System.Windows.Forms.TextBox pTrips;
    }
}