﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

namespace DCDataEntry
{
    class runTimeControlAdder
    {

        private FlowLayoutPanel flowLayoutPanel;
        private int index;
        Func<string[]> dataRetrieveMethod;
        private string labelName;
        DataLoader.DataSource type;
        private string hint;

        public runTimeControlAdder(FlowLayoutPanel flowLayoutPanel, int index, Func<string[]> dataRetrieveMethod,  DataLoader.DataSource type,string labelName, string tooltip)
        {
            this.flowLayoutPanel = flowLayoutPanel;
            this.index = index;
            this.dataRetrieveMethod=dataRetrieveMethod;
            this.labelName = labelName;
            this.type = type;
            this.hint = tooltip;

        }

        public FlowLayoutPanel FlowLayout
        {
            get { return flowLayoutPanel; }
            set { flowLayoutPanel = value; }
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public FlowLayoutPanel loadPanel()
        {
            try
            {
                // clear all
                flowLayoutPanel.Controls.Clear();
                // 1- add label, DDL and button
                Label l = new Label();
                l.Size = new Size(50, 20);
                l.Location = new Point(5, 0);
                l.Text = labelName;
                l.Name = "l" + index;
                l.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);


                
                ComboBox comboBoxItem = new ComboBox();
                comboBoxItem.Size = new Size(140, 20);
                comboBoxItem.Location = new Point(90, 0);
                comboBoxItem.Name = "CB" + index;
                comboBoxItem.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
                comboBoxItem.DropDownStyle = ComboBoxStyle.DropDownList;
                comboBoxItem.SelectedIndexChanged += contributorCB_SelectedIndexChanged;
                //comboBoxItem.Items.AddRange(DataLoader.loadContributor());
                comboBoxItem.Items.AddRange(dataRetrieveMethod());


                Button cbADD = new Button();
                cbADD.Name = "b" + index;
                cbADD.Text = "+";
                cbADD.Location = new Point(240, 0);
                cbADD.Size = new Size(30, 20);
                cbADD.Click += cbADD_Click;
                cbADD.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);


                ToolTip toolTip1 = new ToolTip();
                // Set up the delays for the ToolTip.
                toolTip1.AutoPopDelay = 5000;
                toolTip1.InitialDelay = 1000;
                toolTip1.ReshowDelay = 500;
                // Force the ToolTip text to be displayed whether or not the form is active.
                toolTip1.ShowAlways = true;
              

                // Set up the ToolTip text for the Button and Checkbox.
                toolTip1.SetToolTip(l, this.hint);
                toolTip1.SetToolTip(comboBoxItem, this.hint);
                toolTip1.SetToolTip(cbADD, this.hint);



                Panel p = new Panel();
                p.Name = "p" + index;
                p.Size = new Size(275, 25);
                p.Controls.Add(l);
                p.Controls.Add(comboBoxItem);
                p.Controls.Add(cbADD);
             //   p.Controls.Add(help);

                index++;

                flowLayoutPanel.Controls.Add(p);

            }
            catch (Exception e)
            {
                MessageBox.Show(Constants.loadContributorPanelExp + e.Message);
            }
            return flowLayoutPanel;
        }

   

        void contributorCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox current = (ComboBox)sender;
            if (current.Text == Constants.addNew)
            {
                // add new item
                DataEntry de = new DataEntry();
                de.ShowDialog();
                // add to comboBox
                if (de.Caption != String.Empty)
                {
                    int result = current.FindStringExact(de.Caption);
                    if (result > -1)
                    {
                        MessageBox.Show(Constants.valueAlreadyExists);
                        current.SelectedIndex = result;
                    }
                    else
                    {
                        if (DataLoader.addItem(type, de.Caption) == null)
                        {
                            MessageBox.Show(Constants.addederror);
                            return;
                        }
                        current.Items.Add(de.Caption);
                        current.Text = de.Caption;
                       
                        refreshALLContributorsData(current.Name, de.Caption);
                    }

                }


            }

        }

        //  load the control on fly
        public void cbADD_Click(object sender, EventArgs e)
        {

            if (index > 4)
            {
                MessageBox.Show(Constants.maxCountReached);
                return;
            }

            ComboBox comboBoxItem = new ComboBox();
            comboBoxItem.Size = new Size(140, 20);
            comboBoxItem.Location = new Point(90, 0);
            comboBoxItem.Name = "CB" + index;
            comboBoxItem.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
            comboBoxItem.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxItem.Items.AddRange(dataRetrieveMethod());
            comboBoxItem.SelectedIndexChanged += contributorCB_SelectedIndexChanged;

            Button minusButton = new Button();
            minusButton.Size = new Size(30, 20);
            minusButton.Location = new Point(240, 0);
            minusButton.Name = "M" + index;
            minusButton.Text = "-";
            minusButton.Click += minusButton_Click;


            Panel p = new Panel();
            p.Name = "p" + index;
            p.Size = new Size(275, 25);
            p.Controls.Add(comboBoxItem);
            p.Controls.Add(minusButton);
            index++;

            flowLayoutPanel.Controls.Add(p);



        }

        void minusButton_Click(object sender, EventArgs e)
        {
            string ctrlName = "p" + ((Button)sender).Name.Substring(1);
            foreach (Panel p in flowLayoutPanel.Controls)
            {
                if (p.Name == ctrlName)
                {
                    flowLayoutPanel.Controls.Remove(p);
                    p.Dispose();
                    index--;
                    break;

                }
            }
        }

        void refreshALLContributorsData(string currentCTRLName, string value)
        {
            foreach (Panel p in flowLayoutPanel.Controls)
            {
                foreach (ComboBox cb in p.Controls.OfType<ComboBox>())
                {
                    if (cb.Name != currentCTRLName)
                    {
                        cb.Items.Add(value);

                    }

                }
            }

        }
    }
}
